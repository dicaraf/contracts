<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
 * Agents Controller
 *
 * @property \App\Model\Table\AgentsTable $Agents
 */
class AgentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Contracts.WorkTypeInstances.Payments', 
                ]
        ];
        $agents = $this->paginate($this->Agents);
        
        
        $this->set(compact('agents'));
        $this->set('_serialize', ['agents']);
    }

    /**
     * View method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agent = $this->Agents->get($id, [
            /*'contain' => ['Contracts' => [
                    'sort' => ['Contracts.data_contratto' => 'DESC']
                    ],
                'Contracts.WorkTypeInstances.WorkTypes',
                'Contracts.WorkTypeInstances.Payments',
                'Contracts.WorkTypeInstances.PaymentTypes',
                'Contracts.WorkTypeInstances.Statuses'
                ]*/
        ]);
        $payments = TableRegistry::get('Payments');
        $query = $payments->find('all', [
            'conditions' => ['Payments.payment_type_id' => 3],
            'contain' => ['WorkTypeInstances'],
            'order' => ['Payments.data_scadenza' => 'DESC']
            ]);
        $list = $query->toArray();
        $contractsTable = TableRegistry::get('Contracts');
        $contracts = $contractsTable->find('list')
                                    ->select(['id', 'numero'])
                                    ->where(['agent_id' => $id]);
        $contracts = $contracts->toArray();
        $ordersTable = TableRegistry::get('Orders');
        $orders = $ordersTable->find('list')
                              ->select(['id', 'numero'])
                              ->where(['agent_id' => $id]);
        $orders=$orders->toArray();
        $workTypesTable = TableRegistry::get('WorkTypes');
        $workTypes = $workTypesTable->find('list');
        $workTypes = $workTypes->toArray();
        $clientTable = TableRegistry::get('Clients');
        $clients = $clientTable->find('list');
        $clients = $clients->toArray();
        $now = Time::now();
        $this->set('now', $now);
        $this->set('list', $list);
        $this->set('contracts', $contracts);
        $this->set('orders', $orders);
        $this->set('workTypes', $workTypes);
        $this->set('clients', $clients);
        $this->set('agent', $agent);
        $this->set('_serialize', ['agent']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));
        $agent = $this->Agents->newEntity();
        if ($this->request->is('post')) {
            $agent = $this->Agents->patchEntity($agent, $this->request->data);
            if ($this->Agents->save($agent)) {
                $this->Flash->success(__('The agent has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The agent could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('agent'));
        $this->set('_serialize', ['agent']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));
        $agent = $this->Agents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agent = $this->Agents->patchEntity($agent, $this->request->data);
            if ($this->Agents->save($agent)) {
                $this->Flash->success(__('The agent has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The agent could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('agent'));
        $this->set('_serialize', ['agent']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agent = $this->Agents->get($id);
        if ($this->Agents->delete($agent)) {
            $this->Flash->success(__('The agent has been deleted.'));
        } else {
            $this->Flash->error(__('The agent could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function getProvvigione() {
        $id = $this->request->data['agent-id'];
 
        $agent = $this->Agents->get($id);
        $this->set('provvigione',$agent->provvigione_default);
        $this->viewBuilder()->layout('ajax');
    }
}
