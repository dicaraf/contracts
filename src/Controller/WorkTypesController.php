<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * WorkTypes Controller
 *
 * @property \App\Model\Table\WorkTypesTable $WorkTypes
 */
class WorkTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PaymentTypes']
        ];
        $workTypes = $this->paginate($this->WorkTypes);

        $this->set(compact('workTypes'));
        $this->set('_serialize', ['workTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Work Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $workType = $this->WorkTypes->get($id, [
            'contain' => ['WorkTypeInstances']
        ]);

        $this->set('workType', $workType);
        $this->set('_serialize', ['workType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));

        $workType = $this->WorkTypes->newEntity();
        if ($this->request->is('post')) {
            $workType = $this->WorkTypes->patchEntity($workType, $this->request->data);
            if ($this->WorkTypes->save($workType)) {
                $this->Flash->success(__('The work type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The work type could not be saved. Please, try again.'));
            }
        }
        $payment_types=$this->WorkTypes->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('workType', 'payment_types'));
        $this->set('_serialize', ['workType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Work Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));

        $workType = $this->WorkTypes->get($id, [
            
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $workType = $this->WorkTypes->patchEntity($workType, $this->request->data);
            if ($this->WorkTypes->save($workType)) {
                $this->Flash->success(__('The work type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The work type could not be saved. Please, try again.'));
            }
        }
        //$contracts = $this->WorkTypes->Contracts->find('list', ['limit' => 200]);
        $payment_types=$this->WorkTypes->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('workType', 'payment_types'));
        $this->set('_serialize', ['workType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Work Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));

        $this->request->allowMethod(['post', 'delete']);
        $workType = $this->WorkTypes->get($id);
        if ($this->WorkTypes->delete($workType)) {
            $this->Flash->success(__('The work type has been deleted.'));
        } else {
            $this->Flash->error(__('The work type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function getNomi() {
        $str = $this->request->data['worktype-str'];


        $names = $this->WorkTypes->find('list', [
            'conditions' => ['WorkTypes.nome LIKE' => '%'.$str.'%'],
            'keyField' => 'id', 
            'valueField' => 'nome',
            'recursive' => -1,
        ]);
        $this->set('names',$names);
        $this->viewBuilder()->layout('ajax');
    }
}
