<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * StepTypes Controller
 *
 * @property \App\Model\Table\StepTypesTable $StepTypes
 */
class PaymentTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $paymentTypes = $this->paginate($this->PaymentTypes);

        $this->set(compact('paymentTypes'));
        $this->set('_serialize', ['paymentTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Step Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stepType = $this->PaymentTypes->get($id, [
            'contain' => []
        ]);

        $this->set('paymentType', $stepType);
        $this->set('_serialize', ['paymentType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));

        $paymentType = $this->PaymentTypes->newEntity();
        if ($this->request->is('post')) {
            $paymentType = $this->PaymentTypes->patchEntity($paymentType, $this->request->data);
            if ($this->PaymentTypes->save($paymentType)) {
                $this->Flash->success(__('The payment type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The payment type could not be saved. Please, try again.'));
            }
        }
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('paymentType'));
        $this->set('_serialize', ['paymentType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Step Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));

        $paymentType = $this->PaymentTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stepType = $this->PaymentTypes->patchEntity($paymentType, $this->request->data);
            if ($this->PaymentTypes->save($paymentType)) {
                $this->Flash->success(__('The payment type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The payment type could not be saved. Please, try again.'));
            }
        }
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('paymentType'));
        $this->set('_serialize', ['paymentType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Step Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentType = $this->PaymentTypes->get($id);
        if ($this->PaymentTypes->delete($paymentType)) {
            $this->Flash->success(__('The payment type has been deleted.'));
        } else {
            $this->Flash->error(__('The payment type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
