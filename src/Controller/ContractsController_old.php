<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\ContractsTable;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
/**
 * Contracts Controller
 *
 * @property \App\Model\Table\ContractsTable $Contracts
 */
class ContractsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Agents', 
                'Clients',
                'Steps.Payments',
                ]
        ];
        $contracts = $this->paginate($this->Contracts);

        $this->set(compact('contracts'));
        $this->set('_serialize', ['contracts']);
        
        
        $list = $this->Contracts->find('open');
        $this->set(compact('list'));
        
    }
    

    /**
     * View method
     *
     * @param string|null $id Contract id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contract = $this->Contracts->get($id, [
            'contain' => [
                'Agents',
                'Clients',
                'Steps.Payments',
            ]
        ]);
        
        $fatturatoIn = 0;
        $pagatoIn = 0;
        $fatturatoOut = 0;
        $pagatoOut = 0;
        $provvigione = 0;
        $totaleIn = 0;
        $totale = 0;
        foreach ($contract->steps as $steps){
            foreach ($steps->payments as $payments){
                if($payments->inEntrata){
                    if($payments->fatturato){
                        $fatturatoIn = $fatturatoIn + $payments->importo;
                    }
                    if($payments->payed) {
                        $pagatoIn = $pagatoIn + $payments->importo;
                    }
                    $totaleIn = $totaleIn + $payments->importo;
                }
                else{
                    if($payments->fatturato){
                        $fatturatoOut = $fatturatoOut + $payments->importo;
                    }
                    if($payments->payed) {
                        $pagatoOut = $pagatoOut + $payments->importo;
                    }
                }
            }
            $provvigione = $provvigione + $contract->fisso_agent + $steps->provvigione_agent*$steps->importo/100;
            $totale = $totale + $steps->importo;
        }
        $types = $this->Contracts->Contractjobs->find('list',[
                    'conditions' => ['contract_id' => $id],
                    'keyField' => 'contracttype.id', 
                    'valueField' => 'contracttype.nome', 
                    'limit' => 200])
                    ->contain(['Contracttypes']);
        $this->set(compact('types'));
        $this->set('provvigione', $provvigione);
        $this->set('provvigioneNotPayed', $provvigione-$pagatoOut);
        $this->set('provvigionePayed', $pagatoOut-$fatturatoOut);
        $this->set('provvigioneFatturata', $fatturatoOut);
        $this->set('fatturatoIn', $fatturatoIn);
        $this->set('pagatoIn', $pagatoIn-$fatturatoIn);
        $this->set('totaleInNotPayed', $totaleIn-$pagatoIn);
        $this->set('totale', $totale);
        $this->set('totaleNotInserted', $totale-$totaleIn);
        $this->set('contract', $contract);
        $this->set('_serialize', ['contract']);
        
        
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contract = $this->Contracts->newEntity();
        if ($this->request->is('post')) {
            $contract = $this->Contracts->patchEntity($contract, $this->request->data);
            if ($this->Contracts->save($contract)) {
                $this->Flash->success(__('The contract has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contract could not be saved. Please, try again.'));
            }
        }
        $agents = $this->Contracts->Agents->find('list', ['limit' => 200]);
        $clients = $this->Contracts->Clients->find('list', ['limit' => 200]);
        $this->set(compact('contract', 'agents', 'clients'));
        $this->set('_serialize', ['contract']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contract id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contract = $this->Contracts->get($id, [
            'contain' => [
                'Contractjobs.Contracttypes',
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contract = $this->Contracts->patchEntity($contract, $this->request->data);
            if ($this->Contracts->save($contract)) {
                $this->Flash->success(__('The contract has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contract could not be saved. Please, try again.'));
            }
        }
        $agents = $this->Contracts->Agents->find('list', ['limit' => 200]);
        $clients = $this->Contracts->Clients->find('list', ['limit' => 200]);
        $types = $this->Contracts->Contractjobs->find('list',[
                    'conditions' => ['contract_id' => $id],
                    'keyField' => 'contracttype.id', 
                    'valueField' => 'contracttype.nome', 
                    'limit' => 200])
                    ->contain(['Contracttypes']);
        $this->set(compact('contract', 'types', 'agents', 'clients'));
        $this->set('_serialize', ['contract']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contract id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contract = $this->Contracts->get($id);
        if ($this->Contracts->delete($contract)) {
            $this->Flash->success(__('The contract has been deleted.'));
        } else {
            $this->Flash->error(__('The contract could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
   
}
