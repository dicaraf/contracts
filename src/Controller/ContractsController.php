<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Contracts Controller
 *
 * @property \App\Model\Table\ContractsTable $Contracts
 */
class ContractsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        $contracts = $this->Contracts->find('all', [
            'contain' => [
                'Statuses', 
                'Agents', 
                'Clients',
                'WorkTypeInstances.Payments',
                'WorkTypeInstances.WorkTypes'
                ]
        ]);
        
        //$list = $this->Contracts->find('open');
        $this->set(compact('contracts'));
        //$this->set('list', $list);
        $this->set('_serialize', ['contracts']);
    }

    /**
     * View method
     *
     * @param string|null $id Contract id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contract = $this->Contracts->get($id, [
            'contain' => [
                'Statuses', 
                'Agents', 
                'Clients', 
                'WorkTypeInstances.WorkTypes', 
                'WorkTypeInstances.Payments',
                'WorkTypeInstances.PaymentTypes',
                'WorkTypeInstances.Statuses']
        ]);
        
        
        $this->set('contract', $contract);
        $this->set('_serialize', ['contract']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));

        $contract = $this->Contracts->newEntity();
        if ($this->request->is('post')) {
            $contract = $this->Contracts->patchEntity($contract, $this->request->data);
            
            if ($this->Contracts->save($contract)) {
                $this->Flash->success(__('The contract has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contract could not be saved. Please, try again.'));
            }
        }
        $statuses = $this->Contracts->Statuses->find('list', ['limit' => 200]);
        $agents = $this->Contracts->Agents->find('list', ['limit' => 200]);
        $clients = $this->Contracts->Clients->find('list', ['limit' => 200]);
        $workTypes = $this->Contracts->WorkTypeInstances->WorkTypes->find('list', ['limit' => 200]);
        $paymentTypes = $this->Contracts->WorkTypeInstances->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('contract', 'statuses', 'agents', 'clients', 'workTypes', 'paymentTypes'));
        $this->set('_serialize', ['contract']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contract id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));
        $contract = $this->Contracts->get($id, [
            'contain' => [
                'WorkTypeInstances.WorkTypes',
                'WorkTypeInstances.Payments',
                'WorkTypeInstances.PaymentTypes'
                ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contract = $this->Contracts->patchEntity($contract, $this->request->data);
            if ($this->Contracts->save($contract)) {
                $this->Flash->success(__('The contract has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contract could not be saved. Please, try again.'));
            }
        }
        $statuses = $this->Contracts->Statuses->find('list', ['limit' => 200]);
        $agents = $this->Contracts->Agents->find('list', ['limit' => 200]);
        $clients = $this->Contracts->Clients->find('list', ['limit' => 200]);
        $workTypes = $this->Contracts->WorkTypeInstances->WorkTypes->find('list', ['limit' => 200]);
        $paymentTypes = $this->Contracts->WorkTypeInstances->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('contract', 'statuses', 'agents', 'clients', 'workTypes', 'paymentTypes'));
        $this->set('_serialize', ['contract']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contract id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contract = $this->Contracts->get($id);
        if ($this->Contracts->delete($contract)) {
            $this->Flash->success(__('The contract has been deleted.'));
        } else {
            $this->Flash->error(__('The contract could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
