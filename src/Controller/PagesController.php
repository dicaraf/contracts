<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        
        $payments = TableRegistry::get('Payments');
        $query = $payments->find('all', [
            'conditions' => ['Payments.payment_type_id' => 1],
            'contain' => ['WorkTypeInstances'],
            'order' => ['Payments.data_scadenza' => 'DESC']
            ]);
        $list = $query->toArray();
        $clientsTable = TableRegistry::get('Clients');
        $clients = $clientsTable->find('list');
        $clients = $clients->toArray();
        $agentsTable = TableRegistry::get('Agents');
        $agents = $agentsTable->find('list');
        $agents = $agents->toArray();
        $contractsTable = TableRegistry::get('Contracts');
        $contracts2 = $contractsTable->find('list');
        $contracts2 = $contracts2->toArray();
        $ordersTable = TableRegistry::get('Orders');
        $orders2 = $ordersTable->find('list');
        $orders2=$orders2->toArray();
        $contractsClients = $contractsTable->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'client_id'
                        ]);
        $contractsClients = $contractsClients->toArray();
        $ordersClients = $ordersTable->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'client_id'
                        ]);
        $ordersClients=$ordersClients->toArray();
        $contractsAgents = $contractsTable->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'agent_id'
                        ]);
        $contractsAgents = $contractsAgents->toArray();
        $ordersAgents = $ordersTable->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'agent_id'
                        ]);
        $ordersAgents=$ordersAgents->toArray();
        $contracts = $contractsTable->find('all', [
            'contain' => [
                'Statuses', 
                'Agents', 
                'Clients',
                'WorkTypeInstances.Payments',
                ]
        ]);
        $orders = $ordersTable->find('all', [
            'contain' => [
                'Statuses', 
                'Agents', 
                'Clients',
                'WorkTypeInstances.Payments',
                ]
        ]);
        $totale_competenze = 0;     
        $totale=0;
        $payed=0;
        $notPayed=0;
        $delayed=0;
        $now = Time::now();
        $payedThisYear=[0,0,0,0,0,0,0,0,0,0,0,0];
        $delayedThisYear=[0,0,0,0,0,0,0,0,0,0,0,0];
        $notPayedThisYear=[0,0,0,0,0,0,0,0,0,0,0,0];
        $payScadenza=null;
        $payScaduti=null;
        $provvigioniScadenza=null;
        $competenze=null;
        $mesi = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
        $mesi2 = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        $this->calcolaDashboard($contracts, $totale_competenze,$totale,$payed,$notPayed,$delayed,
                                        $now,$payedThisYear,$delayedThisYear,$notPayedThisYear,
                                        $payScadenza,$payScaduti,$provvigioniScadenza,$competenze);
        $this->calcolaDashboard($orders, $totale_competenze,$totale,$payed,$notPayed,$delayed,
                                        $now,$payedThisYear,$delayedThisYear,$notPayedThisYear,
                                        $payScadenza,$payScaduti,$provvigioniScadenza,$competenze);
        $this->set(compact('list','orders2','contracts2','competenze', 'totale_competenze',
                        'provvigioniScadenza', 'payScadenza', 'totale', 'payed', 
                        'notPayed', 'delayed', 'payedThisYear', 'notPayedThisYear', 
                        'delayedThisYear', 'now', 'payScaduti',
                'ordersAgents', 'ordersClients', 'contractsAgents', 'contractsClients',
                'clients', 'agents', 'mesi', 'mesi2'));
        
        $this->set('_serialize', ['contracts']);
        
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
        
        
    }
    
    private function calcolaDashboard($array, &$totale_competenze,&$totale,&$payed,&$notPayed,&$delayed,
                                        &$now,&$payedThisYear,&$delayedThisYear,&$notPayedThisYear,
                                        &$payScadenza,&$payScaduti,&$provvigioniScadenza,&$competenze)
    {
        foreach($array as $contract){
        foreach($contract->work_type_instances as $type){
            foreach($type->payments as $pay){
              $date = new Time($pay->data_scadenza);
              if($pay->payment_type_id != 3){
                if($date->year===$now->year){
                    if($pay->payed){
                    $payedThisYear[$date->month-1]=$payedThisYear[$date->month-1]+$pay->importo;
                    }
                    else{
                        if($date<$now){
                            $delayedThisYear[$date->month-1]=$delayedThisYear[$date->month-1]+$pay->importo;
                        }
                        else{
                            $notPayedThisYear[$date->month-1]=$notPayedThisYear[$date->month-1]+$pay->importo;
                        }
                    }
                }
                if($date->year===$now->year && $date->month===$now->month){
                    $pay->contract_id = $type->contract_id;
                    $pay->contract_name = $contract->numero;
                    $pay->agent_name = $contract->agent->nome;
                    $pay->agent_id = $contract->agent_id;
                    $pay->client_name = $contract->client->nome;
                    $pay->client_id = $contract->client_id;
                    $payScadenza[] = $pay;
                }
                $totale = $totale + $pay->importo;
                if($pay->payed){
                    $payed=$payed+$pay->importo;
                }
                else{
                    if($date<$now){
                        $pay->contract_id = $type->contract_id;
                        $pay->contract_name = $contract->numero;
                        $pay->agent_name = $contract->agent->nome;
                        $pay->agent_id = $contract->agent_id;
                        $pay->client_name = $contract->client->nome;
                        $pay->client_id = $contract->client_id;
                        $payScaduti[] = $pay;
                        $delayed=$delayed+$pay->importo;
                    }
                    else{
                        $notPayed = $notPayed+$pay->importo;
                    }
                }
              } else {
                  if($date->year===$now->year && $date->month===$now->month && $contract->agent_id!=1){
                    $pay->contract_id = $type->contract_id;
                    $pay->contract_name = $contract->numero;
                    $pay->agent_name = $contract->agent->nome;
                    $pay->agent_id = $contract->agent_id;
                    $provvigioniScadenza[] = $pay;
                }
                /*if($date<$now && !$pay->payed){
                    $pay->contract_id = $type->contract_id;
                    $pay->contract_name = $contract->numero;
                    $pay->agent_name = $contract->agent->nome;
                    $pay->agent_id = $contract->agent_id;
                    $pay->client_name = $contract->client->nome;
                    $pay->client_id = $contract->client_id;
                    $payScaduti[] = $pay;
                 }*/
              }
                
            }
            
            if(!$type->saldato && $type->payment_type_id!=1){ //competenze se non saldato e non ricorrente
                foreach($type->payments as $pay){
                    if($pay->payed && $pay->payment_type_id!=3){
                       $pay->contract_id = $type->contract_id;
                       $pay->contract_name = $contract->numero;
                       $pay->client_name = $contract->client->nome;
                       $pay->client_id = $contract->client_id; 
                       $competenze[] = $pay;
                       $totale_competenze = $totale_competenze + $pay->importo;
                    }
                }
            }
        }
    }
    }
}
