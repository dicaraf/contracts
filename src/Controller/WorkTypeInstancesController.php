<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * WorkTypeInstances Controller
 *
 * @property \App\Model\Table\WorkTypeInstancesTable $WorkTypeInstances
 */
class WorkTypeInstancesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Statuses', 'Contracts', 'WorkTypes']
        ];
        $WorkTypeInstances = $this->paginate($this->WorkTypeInstances);

        $this->set(compact('WorkTypeInstances'));
        $this->set('_serialize', ['WorkTypeInstances']);
    }

    /**
     * View method
     *
     * @param string|null $id Contracts Work Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $workTypeInstance = $this->WorkTypeInstances->get($id, [
            'contain' => ['Statuses', 'Contracts', 'WorkTypes']
        ]);

        $this->set('workTypeInstance', $workTypeInstance);
        $this->set('_serialize', ['workTypeInstance']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));

        $workTypeInstances = $this->WorkTypeInstances->newEntity();
        if ($this->request->is('post')) {
            $workTypeInstances = $this->WorkTypeInstances->patchEntity($WorkTypeInstances, $this->request->data);
            if ($this->WorkTypeInstances->save($workTypeInstances)) {
                $this->Flash->success(__('The contracts work type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contracts work type could not be saved. Please, try again.'));
            }
        }
        $statuses = $this->WorkTypeInstances->Statuses->find('list', ['limit' => 200]);
        $contracts = $this->WorkTypeInstances->Contracts->find('list', ['limit' => 200]);
        $workTypes = $this->WorkTypeInstances->WorkTypes->find('list', ['limit' => 200]);
        $this->set(compact('workTypeInstances', 'statuses', 'contracts', 'workTypes'));
        $this->set('_serialize', ['workTypeInstances']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contracts Work Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));

        $workTypeInstances = $this->WorkTypeInstances->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $workTypeInstances = $this->WorkTypeInstances->patchEntity($WorkTypeInstances, $this->request->data);
            if ($this->WorkTypeInstances->save($WorkTypeInstances)) {
                $this->Flash->success(__('The contracts work type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contracts work type could not be saved. Please, try again.'));
            }
        }
        $statuses = $this->WorkTypeInstances->Statuses->find('list', ['limit' => 200]);
        $contracts = $this->WorkTypeInstances->Contracts->find('list', ['limit' => 200]);
        $workTypes = $this->WorkTypeInstances->WorkTypes->find('list', ['limit' => 200]);
        $this->set(compact('workTypeInstances', 'statuses', 'contracts', 'workTypes'));
        $this->set('_serialize', ['workTypeInstances']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contracts Work Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        
        $this->request->allowMethod(['post', 'delete']);
        if ($this->request->is(['post'])) {
            $id=$this->request->data['id'];
        }
        $workTypeInstances = $this->WorkTypeInstances->get($id);
        if ($this->WorkTypeInstances->delete($workTypeInstances)) {
            $this->Flash->success(__('The contracts work type has been deleted.'));
        } else {
            $this->Flash->error(__('The contracts work type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function addInContract(){
        $this->set('form_templates', Configure::read('Templates'));
        $workTypeId = $this->request->data['workTypeId'];
        $counter = $this->request->data['counter'];
        $statuses = $this->WorkTypeInstances->Statuses->find('list', ['limit' => 200]);
        $paymentTypes = $this->WorkTypeInstances->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $query = $this->WorkTypeInstances->WorkTypes->find()
                ->select(['nome', 'durata', 'p_type_default'])
                ->where(['WorkTypes.id' => $workTypeId]);
        $workType = $query->toArray();
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('workTypeId', 'counter', 'statuses', 'workType', 'paymentTypes'));
        $this->viewBuilder()->layout('ajax');
    }
}
