<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StepTypes Controller
 *
 * @property \App\Model\Table\StepTypesTable $StepTypes
 */
class StepTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $stepTypes = $this->paginate($this->StepTypes);

        $this->set(compact('stepTypes'));
        $this->set('_serialize', ['stepTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Step Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stepType = $this->StepTypes->get($id, [
            'contain' => []
        ]);

        $this->set('stepType', $stepType);
        $this->set('_serialize', ['stepType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $stepType = $this->StepTypes->newEntity();
        if ($this->request->is('post')) {
            $stepType = $this->StepTypes->patchEntity($stepType, $this->request->data);
            if ($this->StepTypes->save($stepType)) {
                $this->Flash->success(__('The step type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The step type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('stepType'));
        $this->set('_serialize', ['stepType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Step Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $stepType = $this->StepTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stepType = $this->StepTypes->patchEntity($stepType, $this->request->data);
            if ($this->StepTypes->save($stepType)) {
                $this->Flash->success(__('The step type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The step type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('stepType'));
        $this->set('_serialize', ['stepType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Step Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stepType = $this->StepTypes->get($id);
        if ($this->StepTypes->delete($stepType)) {
            $this->Flash->success(__('The step type has been deleted.'));
        } else {
            $this->Flash->error(__('The step type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
