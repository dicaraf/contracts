<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Model\Table\PaymentsTable;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\I18n\I18n;
use DateTime;
use Cake\Core\Configure;


/**
 * Payments Controller
 *
 * @property \App\Model\Table\PaymentsTable $Payments
 */
class PaymentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        $payments = $this->Payments->find('all', [
            'contain' => ['WorkTypeInstances.Contracts.Clients', 'WorkTypeInstances.WorkTypes', 'PaymentTypes']
        ]);
        $payments2 = $this->Payments->find('all', [
            'contain' => ['WorkTypeInstances.Orders.Clients', 'WorkTypeInstances.WorkTypes', 'PaymentTypes']
        ]);
        
        /* OTTIENE ULTIMO GIORNO DEL MESE
        $now = Time::now();
        $now2=date('t-m-Y',strtotime($now));
        $now2=new Time($now2);*/
        $now = Time::now();
        $this->set(compact('payments', 'payments2', 'now'));
        $this->set('_serialize', ['payments']);

    }

    /**
     * View method
     *
     * @param string|null $id Payment id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $payment = $this->Payments->get($id, [
            'contain' => ['WorkTypeInstances.Contracts.Clients',
                'PaymentTypes']
        ]);

        $this->set('payment', $payment);
        $this->set('_serialize', ['payment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $this->set('form_templates', Configure::read('Templates'));

        $payment = $this->Payments->newEntity();
        if ($this->request->is('post')) {
            /*pr($this->request->data['data_inserimento']);
            exit;*/
            $payment = $this->Payments->patchEntity($payment, $this->request->data);
            if ($this->Payments->save($payment)) {
                $this->Flash->success(__('The payment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The payment could not be saved. Please, try again.'));
            }
        }
        $contracts = $this->Payments->WorkTypeInstances->Contracts->find('list', 
                    ['keyField' => 'id', 
                    'valueField' => 'numero', 
                    'limit' => 200]);
                    //->contain(['Clients']);
        $workTypeInstances = $this->Payments->WorkTypeInstances->find('list');
        $paymentTypes = $this->Payments->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        //$steps = $this->Payments->Steps->find('list', ['limit' => 200]);
        $this->set(compact('payment', 'clients', 'contracts', 'workTypeInstances', 'paymentTypes'));
        $this->set('_serialize', ['payment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));

        $payment = $this->Payments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $payment = $this->Payments->patchEntity($payment, $this->request->data);
            if ($this->Payments->save($payment)) {
                $this->Flash->success(__('The payment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The payment could not be saved. Please, try again.'));
            }
        }
        $contracts = $this->Payments->WorkTypeInstances->Contracts->find('list', 
                    ['keyField' => 'id', 
                    'valueField' => 'numero', 
                    'limit' => 200]);
                    //->contain(['Clients']);
        $workTypeInstances = $this->Payments->WorkTypeInstances->find('list');
        $paymentTypes = $this->Payments->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        //$steps = $this->Payments->Steps->find('list', ['limit' => 200]);
        $this->set(compact('payment', 'clients', 'contracts', 'workTypeInstances', 'paymentTypes'));
        $this->set('_serialize', ['payment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Payment id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if ($this->request->is(['post'])) {
            $id=$this->request->data['id'];
        }
        $payment = $this->Payments->get($id);
        if ($this->Payments->delete($payment)) {
            $this->Flash->success(__('The payment has been deleted.'));
        } else {
            $this->Flash->error(__('The payment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function scadenza(){
       
        $days = $this->request->data('days');
        
        if(Empty($days)){
            $days = 10;
        }
        $str = '+'.$days.' Days';
        $query = $this->Payments->find('inScadenza', ['str' => $str]);
        $list = $query->toArray();
        $this->set(compact('list'));
        $this->set('days', $days);
    }
    
    public function addInContract(){
        $this->set('form_templates', Configure::read('Templates'));
        $counterT = $this->request->data['counterT'];
        $counterPay = $this->request->data['counterPay'];
        /*$statuses = $this->WorkTypeInstances->Statuses->find('list', ['limit' => 200]);
        $paymentTypes = $this->WorkTypeInstances->PaymentTypes->find('list', ['limit' => 200]);
        $query = $this->WorkTypeInstances->WorkTypes->find()
                ->select(['nome', 'durata', 'p_type_default'])
                ->where(['WorkTypes.id' => $workTypeId]);
        $workType = $query->toArray();*/
        $this->set(compact('counterT', 'counterPay'));
        $this->viewBuilder()->layout('ajax');
    }
    
    public function pay(){
        $id = $this->request->data['id'];
        
        $payment = $this->Payments->get($id, [
            'contain' => ['WorkTypeInstances']
        ]);
        $payment = $this->Payments->patchEntity($payment, $this->request->data);
        if(isset($payment->work_type_instance->contract_id)){
            $contract = $this->Payments->WorkTypeInstances->Contracts->get($payment->work_type_instance->contract_id);
            $payment->provvigione=$contract->provvigione;
        } elseif(isset($payment->work_type_instance->order_id)) {
            $order = $this->Payments->WorkTypeInstances->Orders->get($payment->work_type_instance->order_id);
            $payment->provvigione=$order->provvigione;
        }
        if(isset($this->request->data['ricorrenza'])){
            $payment->ricorrenza = $this->request->data['ricorrenza'];
        }
        if(isset($this->request->data['fattura'])){
            $payment->numero_fattura = $this->request->data['fattura'];
        }
        
        
        if($payment->payed){
            $payment->payed=0;
            $payment->data_pagamento = null;
            $data=$payment->data_pagamento;
            $payment->work_type_instance = null;
        } else {
            $payment->payed=1;
            $payment->data_pagamento = Time::now();
            if($payment->payment_type_id != 1 && $payment->payment_type_id != 3){
                $siblings=$this->Payments->find('all', [
                        'conditions' => ['Payments.work_type_instance_id' => $payment->work_type_instance_id,
                            'Payments.id !=' => $payment->id]
                        ]);
                $siblings = $siblings->toArray();
                //pr($siblings);
                $contractsTable = TableRegistry::get('Contracts');
                $contract=$contractsTable->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'provvigione',
                        'conditions' => ['Contracts.id' => $payment->work_type_instance->contract_id]
                        ]);
                $contract = $contract->toArray();
                //pr($contract);
                $saldato = 1;
                $importo = $payment->importo;
                foreach($siblings as $pay){
                    if($payment->payment_type_id != 1 && $payment->payment_type_id != 3){
                        $importo=$importo+$pay->importo;
                        if(!$pay->payed){
                            $saldato=0;
                        }
                    }
                }
                if($saldato){
                    $payment->work_type_instance->saldato = $saldato;
                    $payment->work_type_instance->status_id = 2;
                    $newPay['importo'] = $payment->work_type_instance->importo * $contract[$payment->work_type_instance->contract_id]/100;
                    $newPay['payed'] = 0;
                    $newPay['payment_type_id'] = 3;
                    $now2=date('t-m-Y',strtotime($payment->data_pagamento));
                    $now2=new Time($now2);
                    $newPay['data_scadenza'] = $now2;
                    $paymentnew = $this->Payments->newEntity();
                    $paymentnew = $this->Payments->patchEntity($paymentnew, $newPay);
                    $paymentnew->work_type_instance_id = $payment->work_type_instance->id;
                    //pr($payment->work_type_instance);
                }
                else{
                    $payment->work_type_instance = null;
                }
            }
            else{
                $payment->work_type_instance = null;
            }
            $data=$payment->data_pagamento->i18nFormat('dd-MM-yyyy');
        }
        
        //pr($payment);
            //exit;
        if ($this->Payments->save($payment)) {
            if(isset($paymentnew)){
                $this->Payments->save($paymentnew);
            }
            if(isset($payment->work_type_instance)){
                $workTypesInstancesTable = $contractsTable = TableRegistry::get('WorkTypeInstances');
                $workTypesInstancesTable->save($payment->work_type_instance);
            }
           
            $esito = $data;
        } else {
            $esito = 0;
        }
        $this->set(compact('esito', 'id'));
        $this->viewBuilder()->layout('ajax');
    }
    
    public function addft(){
        $id = $this->request->data['id'];
        
        $payment = $this->Payments->get($id, [
            'contain' => ['WorkTypeInstances']
        ]);
        $payment = $this->Payments->patchEntity($payment, $this->request->data);
        if(isset($this->request->data['fattura'])){
            $payment->numero_fattura = $this->request->data['fattura'];
        }
        $this->Payments->save($payment);
        $this->set(compact('id'));
        $this->viewBuilder()->layout('ajax');
    }
    
    
}
