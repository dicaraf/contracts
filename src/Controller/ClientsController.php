<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 */
class ClientsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $clients = $this->Clients->find('all');

        $this->set(compact('clients'));
        $this->set('_serialize', ['clients']);
    }

    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => [
                'Contracts' => [
                    'sort' => ['Contracts.status_id' => 'DESC']
                    ],
                'Contracts.WorkTypeInstances.WorkTypes',
                'Contracts.WorkTypeInstances.Payments',
                'Contracts.WorkTypeInstances.PaymentTypes',
                'Contracts.WorkTypeInstances.Statuses',
                'Contracts.Agents',
                'Orders.WorkTypeInstances.WorkTypes',
                'Orders.WorkTypeInstances.Payments',
                'Orders.WorkTypeInstances.PaymentTypes',
                'Orders.WorkTypeInstances.Statuses',
                'Orders.Agents'
            ],
        ]);

        /*$fatturatoIn = 0;
        $pagatoIn = 0;
        $fatturatoOut = 0;
        $pagatoOut = 0;
        $totaleIn = 0;
        $importoContrattiTot = 0;
        $importoStepTot = 0;
        $notPayed = 0;
        foreach ($client->contracts as $contract){
            foreach ($contract->steps as $steps){
                foreach ($steps->payments as $payments){
                    if($payments->inEntrata){
                        if($payments->fatturato){
                            $fatturatoIn = $fatturatoIn + $payments->importo;
                        }
                        if($payments->payed) {
                            $pagatoIn = $pagatoIn + $payments->importo;
                        } else {
                            $notPayed = $notPayed + $payments->importo;
                        }
                        $totaleIn = $totaleIn + $payments->importo;
                    }
                    else{
                        if($payments->fatturato){
                            $fatturatoOut = $fatturatoOut + $payments->importo;
                        }
                        if($payments->payed) {
                            $pagatoOut = $pagatoOut + $payments->importo;
                        }
                    }
                }
                $importoStepTot = $importoStepTot + $steps->importo;
            }
            $importoContrattiTot = $importoContrattiTot + $contract->importo;
        }
        
        $this->set('importoContrattiTot', $importoContrattiTot);
        $this->set('importoStepTot', $importoStepTot);
        $this->set('totaleIn', $totaleIn);
        $this->set('pagatoIn', $pagatoIn-$fatturatoIn);
        $this->set('fatturatoIn', $fatturatoIn);        
        $this->set('notPayed', $notPayed); */
        $this->set('client', $client);
        $this->set('_serialize', ['client']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));

        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->data);
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The client could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));

        $client = $this->Clients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->data);
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The client could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        if ($this->Clients->delete($client)) {
            $this->Flash->success(__('The client has been deleted.'));
        } else {
            $this->Flash->error(__('The client could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function getNomi() {
        $str = $this->request->data['client-str'];


        $names = $this->WorkTypes->find('list', [
            'conditions' => ['Clients.nome LIKE' => '%'.$str.'%'],
            'keyField' => 'id', 
            'valueField' => 'nome',
            'recursive' => -1,
        ]);
        $this->set('names',$names);
        $this->viewBuilder()->layout('ajax');
    }
}
