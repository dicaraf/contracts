<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        $orders = $this->Orders->find('all',[
            'contain' => [
                'Statuses', 
                'Agents', 
                'Clients',
                'WorkTypeInstances.Payments',
                'WorkTypeInstances.WorkTypes',
                ]
        ]);
        
        //$list = $this->Orders->find('open');
        $this->set(compact('orders'));
        //$this->set('list', $list);
        $this->set('_serialize', ['orders']);
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => [
                'Statuses', 
                'Agents', 
                'Clients', 
                'WorkTypeInstances.WorkTypes', 
                'WorkTypeInstances.Payments',
                'WorkTypeInstances.PaymentTypes',
                'WorkTypeInstances.Statuses']
        ]);
        
        
        $this->set('order', $order);
        $this->set('_serialize', ['order']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('form_templates', Configure::read('Templates'));

        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $statuses = $this->Orders->Statuses->find('list', ['limit' => 200]);
        $agents = $this->Orders->Agents->find('list', ['limit' => 200]);
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $workTypes = $this->Orders->WorkTypeInstances->WorkTypes->find('list', ['limit' => 200]);
        $paymentTypes = $this->Orders->WorkTypeInstances->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('order', 'statuses', 'agents', 'clients', 'workTypes', 'paymentTypes'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('form_templates', Configure::read('Templates'));
        $order = $this->Orders->get($id, [
            'contain' => [
                'WorkTypeInstances.WorkTypes',
                'WorkTypeInstances.Payments',
                'WorkTypeInstances.PaymentTypes'
                ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $statuses = $this->Orders->Statuses->find('list', ['limit' => 200]);
        $agents = $this->Orders->Agents->find('list', ['limit' => 200]);
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $workTypes = $this->Orders->WorkTypeInstances->WorkTypes->find('list', ['limit' => 200]);
        $paymentTypes = $this->Orders->WorkTypeInstances->PaymentTypes->find('list', ['keyField' => 'id', 
                    'valueField' => 'nome', 
                    'limit' => 200]);
        $mesi = ['1' => '1 mese',
            '2' => '2 mesi',
            '3' => '3 mesi',
            '4' => '4 mesi',
            '5' => '5 mesi',
            '6' => '6 mesi',
            '7' => '7 mesi',
            '8' => '8 mesi',
            '9' => '9 mesi',
            '10' => '10 mesi',
            '11' => '11 mesi',
            '12' => '12 mesi',
            '18' => '18 mesi',
            '24' => '24 mesi',
            ];
        
        $this->set('mesi', $mesi);
        $this->set(compact('order', 'statuses', 'agents', 'clients', 'workTypes', 'paymentTypes'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
