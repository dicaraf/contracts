<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modifica cliente</h1>
        <h4> <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $client->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]
            )
        ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Modifica dati agente
            </div>
            <div class="panel-body">      
        <?= $this->Form->create($client) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->label('nome', 'Ragione sociale'); ?>
                <?= $this->Form->input('nome', ['label' => false]); ?>
                <?= $this->Form->input('p_iva'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->label('codice', 'Codice cliente'); ?>
                <?= $this->Form->input('codice', ['label' => false]); ?>
                <?= $this->Form->input('email'); ?>            
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('referente'); ?>
                <?= $this->Form->input('telefono'); ?>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-12"><?= $this->Form->input('descrizione'); ?></div>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>