<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi nuovo Cliente</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento dati cliente
            </div>
            <div class="panel-body">
    <?= $this->Form->create($client) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->label('nome', 'Ragione sociale'); ?>
                <?= $this->Form->input('nome', ['label' => false]); ?>
                <?= $this->Form->input('p_iva'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('referente'); ?>
                <?= $this->Form->input('email'); ?>            
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('codice'); ?>
                <?= $this->Form->input('telefono'); ?>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-12"><?= $this->Form->input('descrizione'); ?></div>
        </div>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>