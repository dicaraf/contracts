<?php

/*$importoNoStep = $importoContrattiTot-$importoStepTot;
$importoNoPayment = $importoStepTot-$totaleIn;

$this->Html->scriptStart(['block' => true]);
echo 'var data = [
    { label: "Importo senza step",  data: "'.$importoNoStep.'", color: "#4572A7"},
    { label: "Importo senza pagamento",  data: "'.$importoNoPayment.'", color: "#260087"},
    { label: "Pagato (non fatturato)",  data: "'.$pagatoIn.'", color: "#80699B"},
    { label: "Fatturato",  data: "'.$fatturatoIn.'", color: "#AA4643"},
    { label: "Non pagato",  data: "'.$notPayed.'", color: "#123456"},
];
 
$(document).ready(function () {
    $.plot($("#flot-pie-chart"), data, {
         series: {
            pie: {
                show: true
            }
         },
         legend: {
            show: false
         }
    });
});';
$this->Html->scriptEnd();*/
?>

<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header"><?= h($client->nome) ?></h1>
    </div>
    <div class="col-lg-3">
        <?= $this->Html->link(__('Edit Client'), ['action' => 'edit', $client->id], ['class'=>'page-header pull-right']) ?> 
    </div>
    <div class="col-lg-3">
        <?= $this->Form->postLink(__('Delete Client'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id), 'class'=>'page-header pull-right']) ?> 
    </div>
</div>
<div class="row">
    
    <table class="table table-striped">
        <tr>
            <th><?= __('Ragione sociale') ?></th>
            <td><?= h($client->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Codice') ?></th>
            <td><?= h($client->codice) ?></td>
        </tr>
        <tr>
            <th><?= __('P Iva') ?></th>
            <td><?= h($client->p_iva) ?></td>
        </tr>
        <tr>
            <th><?= __('Referente') ?></th>
            <td><?= h($client->referente) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($client->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Telefono') ?></th>
            <td><?= h($client->telefono) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrizione') ?></th>
            <td><?= h($client->descrizione) ?></td>
        </tr>
    </table>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Contratti del cliente
                </div>
                <!-- .panel-heading -->
                <div class="panel-body">
                    <div class="panel-group" id="accordion2">
                        <?php 
                        foreach($client->contracts as $contract){
                            (!isset($contract->data_contratto)) ? $data_contratto= '': $data_contratto = ' del '.date_format($contract->data_contratto, 'd-m-Y');
                            echo '<div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                <div class="col-lg-8">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2'.$contract->id.'">Contratto '.$contract->numero.$data_contratto.'</a>
                                </h4>
                                </div>
                                <div class="col-lg-4">';
                                    echo $this->Html->link(__('Edit contratto'), ['controller' => 'Contracts','action' => 'edit', $contract->id]);
                        echo   '</div></div>
                            </div>
                            <div id="collapse2'.$contract->id.'" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav nav-tabs">';
                                        foreach($contract->work_type_instances as $type){
                                            echo '<li><a href="#type'.$type->id.'" data-toggle="tab">'.$type->work_type->nome.'</a>
                                                    </li>';
                                        }
                              echo '</ul>
                                  <div class="tab-content">';
                                        foreach($contract->work_type_instances as $type){
                                            $payed=0;
                                            $notPayed=0;
                                            $delayed=0;
                                            $provvigioniPayed=0;
                                            $provvigioniNotPayed=0;
                                        echo '<div class="tab-pane fade" id="type'.$type->id.'">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="alert alert-danger alert-dismissable" id="ritardo'.$type->id.'" style="display: none">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                            Questa istanza presenta pagamenti scaduti non ancora saldati!
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                Pagamenti
                                                            </div>
                                                            <!-- .panel-heading -->
                                                            <div class="panel-body">
                                                            <div class="table-responsive">
                                                            <table class="table ">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Fatt</th>
                                                                        <th>Importo</th>
                                                                        <th>Scadenza</th>
                                                                        <th>Pagamento</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';
                                                                    foreach($type->payments as $pay){
                                                                        if($pay->payment_type_id!=3){
                                                                            if($pay->payed){
                                                                                $payed=$payed+$pay->importo;
                                                                                $panelType = "success";
                                                                            }
                                                                            else{
                                                                                $now = new DateTime();
                                                                                $date = new DateTime($pay->data_scadenza);
                                                                                    if($date<$now){
                                                                                        $delayed=$delayed+$pay->importo;
                                                                                        $panelType = "danger";
                                                                                    }
                                                                                    else{
                                                                                        $notPayed = $notPayed+$pay->importo;
                                                                                        $panelType = "warning";
                                                                                    }
                                                                            }
                                                                            echo '<tr class="row-'.$panelType.'">
                                                                                <td>'.$pay->numero_fattura.'</td>
                                                                                 <td>€ '.$pay->importo.'</td>';
                                                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'd-m-Y');
                                                                        echo '<td>'.$data_scadenza.'</td>';
                                                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                                                                        echo '<td>'.$data_pagamento.'</td>
                                                                                                    </tr>';
                                                                            }
                                                                        }
                                                                echo    '</tbody>
                                                                          </table>
                                                                         </div>
                                                                     <!-- /.table-responsive -->
                                                                    </div>
                                                                    <!-- .panel-body -->
                                                                </div>
                                                                <!-- /.panel -->
                                                                </div>
                                                                
                                                                <div class="col-lg-6">
                                                                <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Provvigioni - '; echo $this->Html->link($contract->agent->nome, ['controller' => 'Agents', 'action' => 'view', $contract->agent->id]);
                                                echo '</div>
                                                <!-- .panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Importo</th>
                                                                        <th>Scadenza</th>
                                                                        <th>Pagamento</th>
                                                                        <th>Pagato</th>
                                                                        <th>Fatt</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';
                                            foreach($type->payments as $pay){
                                                $panelType = "default";
                                                if($pay->payment_type_id===3){
                                                    if($pay->payed){
                                                        $provvigioniPayed=$provvigioniPayed+$pay->importo;
                                                        $panelType = "success";
                                                    }
                                                    else{
                                                        /*$now = new DateTime();
                                                        $date = new DateTime($pay->data_scadenza);
                                                        if($date<$now){
                                                            $delayed=$delayed+$pay->importo;
                                                            $panelType = "danger";
                                                        }
                                                        else{*/
                                                            $provvigioniNotPayed = $provvigioniNotPayed+$pay->importo;
                                                            $panelType = "warning";
                                                        //}
                                                    }
                                                echo '<tr class="row-'.$panelType.'">
                                                                        <td>€ '.$pay->importo.'</td>';
                                                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'm-Y');
                                                                        echo '<td>'.$data_scadenza.'</td>';
                                                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'm-Y');
                                                                        echo '<td id="pay'.$pay->id.'scadenza">'.$data_pagamento.'</td>
                                                                        <td><div class="col-lg-6" id="pay'.$pay->id.'payed">';
                                                                            if($pay->payed){
                                                                                echo '<span class="glyphicon glyphicon-ok"></span></div>
                                                                                    <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'" checked></input></div>';
                                                                            } else {
                                                                                echo '<span class="glyphicon glyphicon-remove"></span></div>
                                                                                <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                                                                          
                                                                            }
                                                            echo '</td>
                                                                <td>';
                                                                echo $this->Form->input($pay->id, ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                'id' => 'pay'.$pay->id.'fat', 'style'=>'width: 50px', 'class'=>'num_ft2']);
                                                            
                                                            echo '</td>
                                                                    </tr>';
                                                }
                                            }
                                        echo    '</tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                            </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                        
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Payment Type</td>
                                                                                    <td>'.$type->payment_type->nome.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Status</td>
                                                                                    <td>'.$type->status->nome.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Importo</td>
                                                                                    <td>€ '.$type->importo.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Descrizione</td>
                                                                                    <td>'.$type->descrizione.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Pagato</td>
                                                                                    <td>€ '.$payed.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Da pagare</td>';
                                                                                    $notPayedT = $notPayed + $delayed;
                                                                            echo   '<td>€ '.$notPayedT.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>In ritardo</td>
                                                                                    <td>€ '.$delayed.'</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>

                                    </div>
                                    
                                    </div>';
                            }
                        
                                   echo' </div>
                                </div>
                            </div>
                        </div>';
                        }
                    ?>
                    </div>
                </div>
                <!-- .panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ordini del cliente
                </div>
                <!-- .panel-heading -->
                <div class="panel-body">
                    <div class="panel-group" id="accordion5">
                        <?php 
                        foreach($client->orders as $contract){
                            (!isset($contract->data_contratto)) ? $data_contratto= '': $data_contratto = ' del '.date_format($contract->data_contratto, 'd-m-Y');
                            echo '<div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                <div class="col-lg-8">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion5" href="#collapse2'.$contract->id.'">Contratto '.$contract->numero.$data_contratto.'</a>
                                </h4>
                                </div>
                                <div class="col-lg-4">';
                                    echo $this->Html->link(__('Edit contratto'), ['controller' => 'Contracts','action' => 'edit', $contract->id]);
                        echo   '</div></div>
                            </div>
                            <div id="collapse2'.$contract->id.'" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav nav-tabs">';
                                        foreach($contract->work_type_instances as $type){
                                            echo '<li><a href="#type'.$type->id.'" data-toggle="tab">'.$type->work_type->nome.'</a>
                                                    </li>';
                                        }
                              echo '</ul>
                                  <div class="tab-content">';
                                        foreach($contract->work_type_instances as $type){
                                            $payed=0;
                                            $notPayed=0;
                                            $delayed=0;
                                            $provvigioniPayed=0;
                                            $provvigioniNotPayed=0;
                                        echo '<div class="tab-pane fade" id="type'.$type->id.'">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="alert alert-danger alert-dismissable" id="ritardo'.$type->id.'" style="display: none">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                            Questa istanza presenta pagamenti scaduti non ancora saldati!
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                Pagamenti
                                                            </div>
                                                            <!-- .panel-heading -->
                                                            <div class="panel-body">
                                                            <div class="table-responsive">
                                                            <table class="table ">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Fatt</th>
                                                                        <th>Importo</th>
                                                                        <th>Scadenza</th>
                                                                        <th>Pagamento</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';
                                                                    foreach($type->payments as $pay){
                                                                        if($pay->payment_type_id!=3){
                                                                            if($pay->payed){
                                                                                $payed=$payed+$pay->importo;
                                                                                $panelType = "success";
                                                                            }
                                                                            else{
                                                                                $now = new DateTime();
                                                                                $date = new DateTime($pay->data_scadenza);
                                                                                    if($date<$now){
                                                                                        $delayed=$delayed+$pay->importo;
                                                                                        $panelType = "danger";
                                                                                    }
                                                                                    else{
                                                                                        $notPayed = $notPayed+$pay->importo;
                                                                                        $panelType = "warning";
                                                                                    }
                                                                            }
                                                                            echo '<tr class="row-'.$panelType.'">
                                                                                <td>'.$pay->numero_fattura.'</td>
                                                                                 <td>€ '.$pay->importo.'</td>';
                                                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'd-m-Y');
                                                                        echo '<td>'.$data_scadenza.'</td>';
                                                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                                                                        echo '<td>'.$data_pagamento.'</td>
                                                                                                    </tr>';
                                                                            }
                                                                        }
                                                                echo    '</tbody>
                                                                          </table>
                                                                         </div>
                                                                     <!-- /.table-responsive -->
                                                                    </div>
                                                                    <!-- .panel-body -->
                                                                </div>
                                                                <!-- /.panel -->
                                                                </div>
                                                                
                                                                <div class="col-lg-6">
                                                                <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Provvigioni - '; echo $this->Html->link($contract->agent->nome, ['controller' => 'Agents', 'action' => 'view', $contract->agent->id]);
                                                echo '</div>
                                                <!-- .panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Importo</th>
                                                                        <th>Scadenza</th>
                                                                        <th>Pagamento</th>
                                                                        <th>Pagato</th>
                                                                        <th>Fatt</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';
                                            foreach($type->payments as $pay){
                                                $panelType = "default";
                                                if($pay->payment_type_id===3){
                                                    if($pay->payed){
                                                        $provvigioniPayed=$provvigioniPayed+$pay->importo;
                                                        $panelType = "success";
                                                    }
                                                    else{
                                                        /*$now = new DateTime();
                                                        $date = new DateTime($pay->data_scadenza);
                                                        if($date<$now){
                                                            $delayed=$delayed+$pay->importo;
                                                            $panelType = "danger";
                                                        }
                                                        else{*/
                                                            $provvigioniNotPayed = $provvigioniNotPayed+$pay->importo;
                                                            $panelType = "warning";
                                                        //}
                                                    }
                                                echo '<tr class="row-'.$panelType.'">
                                                                        <td>€ '.$pay->importo.'</td>';
                                                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'm-Y');
                                                                        echo '<td>'.$data_scadenza.'</td>';
                                                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'm-Y');
                                                                        echo '<td id="pay'.$pay->id.'scadenza">'.$data_pagamento.'</td>
                                                                        <td><div class="col-lg-6" id="pay'.$pay->id.'payed">';
                                                                            if($pay->payed){
                                                                                echo '<span class="glyphicon glyphicon-ok"></span></div>
                                                                                    <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'" checked></input></div>';
                                                                            } else {
                                                                                echo '<span class="glyphicon glyphicon-remove"></span></div>
                                                                                <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                                                                          
                                                                            }
                                                            echo '</td>
                                                                <td>';
                                                            
                                                                echo $this->Form->input($pay->id, ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                'id' => 'pay'.$pay->id.'fat', 'style'=>'width: 50px', 'class'=>'num_ft2']);
                                                            
                                                            echo '</td>
                                                                    </tr>';
                                                }
                                            }
                                        echo    '</tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                            </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                        
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Payment Type</td>
                                                                                    <td>'.$type->payment_type->nome.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Status</td>
                                                                                    <td>'.$type->status->nome.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Importo</td>
                                                                                    <td>€ '.$type->importo.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Descrizione</td>
                                                                                    <td>'.$type->descrizione.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Pagato</td>
                                                                                    <td>€ '.$payed.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Da pagare</td>';
                                                                                    $notPayedT = $notPayed + $delayed;
                                                                            echo   '<td>€ '.$notPayedT.'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>In ritardo</td>
                                                                                    <td>€ '.$delayed.'</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>

                                    </div>
                                    
                                    </div>';
                            }
                        
                                   echo' </div>
                                </div>
                            </div>
                        </div>';
                        }
                    ?>
                    </div>
                </div>
                <!-- .panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

