<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Clienti</h1>
    </div>
    <div class="col-lg-6">
        <?= $this->Html->link(__('Aggiungi Cliente'), ['controller' => 'Clients', 'action' => 'add'], ['class'=>'page-header pull-right']) ?>
    </div>
</div>
    <table id="table-index" class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Codice</th>
                <th>Ragione sociale</th>
                <th>Partita IVA</th>
                <th>Referente</th>
                <th>Email</th>
                <th>Telefono</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clients as $client): ?>
            <tr>
                <td><?= h($client->codice) ?></td>
                <td><?= h($client->nome) ?></td>
                <td><?= h($client->p_iva) ?></td>
                <td><?= h($client->referente) ?></td>
                <td><?= h($client->email) ?></td>
                <td><?= h($client->telefono) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $client->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $client->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
</div>
