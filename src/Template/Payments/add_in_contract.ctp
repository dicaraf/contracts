<?php

echo '<fieldset>';
            $this->Form->templates($form_templates['fullForm']);   
            echo '<div class="row">
            <div class="col-lg-4">';
            echo $this->Form->input('work_type_instances.'.$counterT.'.payments.'.$counterPay.'.importo', ['required' => 'required', 'placeholder' => '€']);
            echo $this->Form->input('work_type_instances.'.$counterT.'.payments.'.$counterPay.'.payed', ['type' => 'checkbox']);
            echo '</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counterT.'-payments-'.$counterPay.'-data_scadenza">Data scadenza</label>
                    <div class="input-group date data_normale">
                        <input type="text" class="form-control" required="required" name="work_type_instances['.$counterT.'][payments]['.$counterPay.'][data_scadenza]"  readonly/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
            echo $this->Form->label('work_type_instances.'.$counterT.'.payments.'.$counterPay.'.numero_fattura', 'Numero fattura');
            echo $this->Form->input('work_type_instances.'.$counterT.'.payments.'.$counterPay.'.numero_fattura', ['empty' => true, 'label' => false,
                                                                                                                    'id' => 'work-type-instances-'.$counterT.'-payments-'.$counterPay.'-payedfat']);
            echo $this->Form->hidden('work_type_instances.'.$counterT.'.payments.'.$counterPay.'.payment_type_id', ['value' => 0]);    
            echo '</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counterT.'-payments-'.$counterPay.'-data_pagamento">Data pagamento</label>
                    <div class="input-group date data_normale">
                        <input type="text" class="form-control" name="work_type_instances['.$counterT.'][payments]['.$counterPay.'][data_pagamento]" id="work-type-instances-'.$counterT.'-payments-'.$counterPay.'-payedday" readonly disabled />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div></div>
                    <p class="removePayment" name="'.$counterT.'pay'.$counterPay.'"> Delete pagamento <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></p>';
            
            echo '</div></div></fieldset>';

?>
