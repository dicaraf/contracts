<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="payments index large-9 medium-8 columns content">
    <h3><?= __('Payments in scadenza in '.$days.' giorni') ?>
<?php
echo $this->Form->create(null, ['url' => ['action' => 'scadenza']]);
echo $this->Form->label('days', 'Cambia numero giorni per vedere altre scadenze');
echo $this->Form->input('days', ['label' => false]);
echo $this->Form->button('Vedi scadenze', ['class' => 'btn btn-default','type' => 'submit']);
echo $this->Form->end();
$list = isset($list) 
        ? $list 
        : null;
?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('step_id') ?></th>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th><?= $this->Paginator->sort('importo') ?></th>
                <th><?= $this->Paginator->sort('fatturato') ?></th>
                <th><?= $this->Paginator->sort('payed') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ($list != null): ?>
            <?php foreach ($list as $payment): ?>
            <?php if ($payment->inEntrata) {
                $rowclass = 'inPayment';
            } else {
                $rowclass = 'outPayment';
            }
            ?>
            <tr class=<?= $rowclass ?> >
                <td><?= $this->Number->format($payment->id) ?></td>
                <td><?= $this->Html->link($payment->step_id, ['controller' => 'Steps', 'action' => 'view', $payment->step_id]) ?></td>
                <td><?= h($payment->date) ?></td>
                <td>€ <?= $this->Number->format($payment->importo) ?></td>
                <td><?= h($payment->fatturato) ?></td>
                <td><?= h($payment->payed) ?></td>
                <td><?= h($payment->created) ?></td>
                <td><?= h($payment->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $payment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $payment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $payment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $payment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    
</div>
