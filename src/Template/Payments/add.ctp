<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi nuovo pagamento</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento dati pagamento
            </div>
            <div class="panel-body">
    <?= $this->Form->create($payment) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?php echo $this->Form->input('payment_type_id', ['options' => $paymentTypes]); ?>
                <div class="form-group">
                    <label for="data_pagamento">Data inserimento</label>
                    <div class='input-group date' >
                        <input type='text' class="form-control" name="data_inserimento" value="<?= $payment->data_inserimento ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <?php echo $this->Form->input('payed'); ?>
            </div>
            
            <div class="col-lg-4">
                <?php echo $this->Form->input('importo'); ?>
                <div class="form-group">
                    <label for="data_scadenza">Data scadenza pagamento</label>
                    <div class='input-group date'>
                        <input type='text' class="form-control" name="data_scadenza" value="<?= $payment->data_scadenza ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <?php
                    echo $this->Form->input('work_type_instance_id', ['options' => $workTypeInstances]);
                ?>
                <div class="form-group">
                    <label for="data_pagamento">Data pagamento</label>
                    <div class='input-group date'>
                        <input type='text' class="form-control" name="data_pagamento" value="<?= $payment->data_pagamento ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        
        
        
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
