<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= h('Pagamento '.$contract->numero) ?></h1>
        <h4><?= $this->Html->link(__('Edit pagamento'), ['action' => 'edit', $payment->id]) ?> </h4>
        <h4><?= $this->Form->postLink(__('Delete pagamento'), ['action' => 'delete', $payment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $payment->id)]) ?> </h4>
    </div>
</div>
    <div class="panel-body">

    <table class="table table-striped">
        <tr>
            <th><?= __('Contract') ?></th>
            <td><?= $payment->has('step') ? $this->Html->link($payment->step->contract->client->nome.$payment->step->contract->id, ['controller' => 'Contracts', 'action' => 'view', $payment->step->contract->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Step') ?></th>
            <td><?= $payment->has('step') ? $this->Html->link($payment->step->id, ['controller' => 'Steps', 'action' => 'view', $payment->step->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($payment->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Importo') ?></th>
            <td>€ <?= $this->Number->format($payment->importo) ?></td>
        </tr>
        <tr>
            <th><?= __('Data scadenza') ?></th>
            <td><?= h($payment->date) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($payment->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($payment->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('InEntrata') ?></th>
            <td><?= $payment->inEntrata ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Fatturato') ?></th>
            <td><?= $payment->fatturato ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Payed') ?></th>
            <td><?= $payment->payed ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>

