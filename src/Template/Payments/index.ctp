<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Pagamenti</h1>
    </div>
    
</div>
<div class="row">
<div class="col-lg-12">
<div class="table-responsive">
    <table id="table-index" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Contratto</th>
                <th>Tipo di lavoro</th>
                <th>Tipo di pagamento</th>
                <th>Importo</th>
                <th>Scadenza</th>
                <th>Pagamento</th>
                <th>Fattura</th>
                <th>Pagato</th>
                <th>Edita</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($payments as $payment): ?>
            <?php (!isset($payment->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($payment->data_scadenza, 'd-m-Y');
                    (!isset($payment->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($payment->data_pagamento, 'd-m-Y');
                   if($payment->payed){
                       $class="row-success";
                   }
                   else{
                       if($payment->data_scadenza<$now){
                           $class="row-danger";
                       } else{
                           $class="row-warning";
                       }
                   }
            ?>
            <tr class="<?=$class?>">
                <td><?= $this->Html->link($payment->work_type_instance->contract->numero, ['controller' => 'Contracts', 'action' => 'view', $payment->work_type_instance->contract->id]) ?></td>
                <td><?= $this->Html->link($payment->work_type_instance->work_type->nome, ['controller' => 'Contracts', 'action' => 'view', $payment->work_type_instance->contract->id]) ?></td>
                <td><?= $payment->payment_type->nome?></td>
                <td>€ <?= $this->Number->format($payment->importo) ?></td>
                <td><?= h($data_scadenza) ?></td>
                <td><?= h($data_pagamento) ?></td>
                <td><?= $payment->numero_fattura ?></td>
                <td><?= $payment->payed ?  '<span class="glyphicon glyphicon-ok"></span>*' :  '<span class="glyphicon glyphicon-remove"></span>' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['controller'=>'Contracts','action' => 'edit', $payment->work_type_instance->contract->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php foreach ($payments2 as $payment): ?>
            <?php (!isset($payment->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($payment->data_scadenza, 'm-Y');
                    (!isset($payment->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($payment->data_pagamento, 'd-m-Y');
                    if($payment->payed){
                       $class="row-success";
                   }
                   else{
                       if($payment->data_scadenza<$now){
                           $class="row-danger";
                       } else{
                           $class="row-warning";
                       }
                   }
            ?>
            <tr class="<?=$class?>">
                <td><?= $this->Html->link($payment->work_type_instance->order->numero, ['controller' => 'Orders', 'action' => 'view', $payment->work_type_instance->order->id]) ?></td>
                <td><?= $this->Html->link($payment->work_type_instance->work_type->nome, ['controller' => 'Orders', 'action' => 'view', $payment->work_type_instance->order->id]) ?></td>
                <td><?= $payment->payment_type->nome?></td>
                <td>€ <?= $this->Number->format($payment->importo) ?></td>
                <td><?= h($data_scadenza) ?></td>
                <td><?= h($data_pagamento) ?></td>
                <td><?= $payment->numero_fattura ?></td>
                <td><?= $payment->payed ?  '<span class="glyphicon glyphicon-ok"></span>*' :  '<span class="glyphicon glyphicon-remove"></span>' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['controller'=>'Orders','action' => 'edit', $payment->work_type_instance->order->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
</div>
</div>
    
