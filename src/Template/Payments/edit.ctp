<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit pagamento</h1>
        <h4> <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $payment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $payment->id)]
            )
        ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Modifica pagamento
            </div>
            <div class="panel-body">
    <?= $this->Form->create($payment) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?php echo $this->Form->input('payment_type_id', ['options' => $paymentTypes]); ?>
                <div class="form-group">
                    <label for="data_pagamento">Data inserimento</label>
                    <div class='input-group data_normale' >
                        <input type='text' class="form-control" name="data_inserimento" value="<?= date_format($payment->data_inserimento, 'Y-m-d H:i:s') ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4">
                <?php echo $this->Form->input('importo'); ?>
                <div class="form-group">
                    <label for="data_scadenza">Data scadenza pagamento</label>
                    <div class='input-group data_normale'>
                        <input type='text' class="form-control" name="data_scadenza" value="<?= date_format($payment->data_scadenza, 'Y-m-d H:i:s') ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <?php echo $this->Form->input('numero_fattura'); ?>
                <?php echo $this->Form->input('payed'); ?>
                <div class="form-group">
                    <label for="data_pagamento">Data pagamento</label>
                    <div class='input-group data_normale'>
                        <input type='text' class="form-control" name="data_pagamento" value="<?= date_format($payment->data_pagamento, 'Y-m-d H:i:s') ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        <?php
            echo $this->Form->hidden('work_type_instance_id', ['options' => $workTypeInstances]);
        ?>
        
        
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
