<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi nuovo status</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento dati status
            </div>
            <div class="panel-body">
    <?= $this->Form->create($status) ?>
    <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-6">
                <?= $this->Form->input('nome'); ?>
            </div>
            <div class="col-lg-6">
                <?= $this->Form->input('codice'); ?>
            </div>         
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div></div></div></div>
