<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= h('Status '.$status->numero) ?></h1>
        <h4><?= $this->Html->link(__('Modifica status'), ['action' => 'edit', $status->id]) ?> </h4>
        <h4><?= $this->Form->postLink(__('Delete status'), ['action' => 'delete', $status->id], ['confirm' => __('Are you sure you want to delete # {0}?', $status->id)]) ?> </h4>
    </div>
</div>
    <div class="panel-body">

    <table class="table table-striped">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($status->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Codice') ?></th>
            <td><?= h($status->codice) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($status->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Contracts') ?></h4>
        <?php if (!empty($status->contracts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Status Id') ?></th>
                <th><?= __('Numero') ?></th>
                <th><?= __('Importo') ?></th>
                <th><?= __('Note') ?></th>
                <th><?= __('Agent Id') ?></th>
                <th><?= __('Client Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Provvigione') ?></th>
                <th><?= __('Bonus') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($status->contracts as $contracts): ?>
            <tr>
                <td><?= h($contracts->id) ?></td>
                <td><?= h($contracts->status_id) ?></td>
                <td><?= h($contracts->numero) ?></td>
                <td>€ <?= h($contracts->importo) ?></td>
                <td><?= h($contracts->note) ?></td>
                <td><?= h($contracts->agent_id) ?></td>
                <td><?= h($contracts->client_id) ?></td>
                <td><?= h($contracts->created) ?></td>
                <td><?= h($contracts->modified) ?></td>
                <td><?= h($contracts->provvigione) ?>%</td>
                <td><?= h($contracts->bonus) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Contracts', 'action' => 'view', $contracts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Contracts', 'action' => 'edit', $contracts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Contracts', 'action' => 'delete', $contracts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contracts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Contracts Work Types') ?></h4>
        <?php if (!empty($status->contracts_work_types)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Status Id') ?></th>
                <th><?= __('Contract Id') ?></th>
                <th><?= __('Work Type Id') ?></th>
                <th><?= __('Importo') ?></th>
                <th><?= __('Saldato') ?></th>
                <th><?= __('Descrizione') ?></th>
                <th><?= __('Data Inizio') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($status->contracts_work_types as $WorkTypeInstances): ?>
            <tr>
                <td><?= h($WorkTypeInstances->id) ?></td>
                <td><?= h($WorkTypeInstances->status_id) ?></td>
                <td><?= h($WorkTypeInstances->contract_id) ?></td>
                <td><?= h($WorkTypeInstances->work_type_id) ?></td>
                <td>€ <?= h($WorkTypeInstances->importo) ?></td>
                <td><?= h($WorkTypeInstances->saldato) ?></td>
                <td><?= h($WorkTypeInstances->descrizione) ?></td>
                <td><?= h($WorkTypeInstances->data_inizio) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'WorkTypeInstances', 'action' => 'view', $WorkTypeInstances->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'WorkTypeInstances', 'action' => 'edit', $WorkTypeInstances->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'WorkTypeInstances', 'action' => 'delete', $WorkTypeInstances->id], ['confirm' => __('Are you sure you want to delete # {0}?', $WorkTypeInstances->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
