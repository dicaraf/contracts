<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Contratti</h1>
    </div>
    <div class="col-lg-6">
        <?= $this->Html->link(__('Aggiungi Contratto'), ['controller' => 'Contracts', 'action' => 'add'], ['class'=>'page-header pull-right']) ?>
    </div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="table-responsive">
    <table id="table-index" class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Status</th>
                <th>Importo</th>
                <th>Data contratto</th>
                <th>Cliente</th>
                <th>Agente</th>
                <th>Provvigione</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contracts as $contract): ?>
            <?php 
            $testo='';
            foreach ($contract->work_type_instances as $type){
                $testo = $testo.$type->work_type->codice.':'.$type->importo.'€-';
            }
            ?>
            <tr>
                <td><?= h($contract->numero) ?></td>
                <td><?= $contract->has('status') ? $this->Html->link($contract->status->nome, ['controller' => 'Statuses', 'action' => 'view', $contract->status->id]) : '' ?></td>
                <td><div class="col-lg-6">€ <?= h($contract->importo) ?></div><div class="col-lg-6"><button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="<?=$testo?>"><span class="glyphicon glyphicon-info-sign"></span></button></div></td>
                <td><?= isset($contract->data_contratto) ? date_format($contract->data_contratto, 'd-m-Y') : '' ?></td>
                <td><?= $contract->has('client') ? $this->Html->link($contract->client->nome, ['controller' => 'Clients', 'action' => 'view', $contract->client->id]) : '' ?></td>
                <td><?= $contract->has('agent') ? $this->Html->link($contract->agent->nome, ['controller' => 'Agents', 'action' => 'view', $contract->agent->id]) : '' ?></td>
                <td><?= $this->Number->format($contract->provvigione) ?>%</td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contract->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contract->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contract->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contract->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
</div>
</div>
</div>  
