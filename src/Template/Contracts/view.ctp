<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header"><?= 'Contratto '.$contract->numero.' del '.date_format($contract->data_contratto, 'd-m-Y') ?></h1>
    </div>
    <div class="col-lg-3">
        <?= $this->Html->link(__('Edit Contract'), ['action' => 'edit', $contract->id], ['class'=>'page-header pull-right']) ?> 
    </div>
    <div class="col-lg-3">
        <?= $this->Form->postLink(__('Delete Contract'), ['action' => 'delete', $contract->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contract->id), 'class'=>'page-header pull-right']) ?> 
    </div>
</div>
    <div class="panel-body">
        <div class="alert alert-danger alert-dismissable" id="scaduti" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Questo contratto presenta pagamenti scaduti non ancora saldati!
        </div>
    <table class="table table-striped">
        <tr>
            <th><?= __('Numero') ?></th>
            <td><?= h($contract->numero) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $contract->has('status') ? $this->Html->link($contract->status->nome, ['controller' => 'Statuses', 'action' => 'view', $contract->status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipi di lavoro') ?></th>
            <td><?php foreach($contract->work_type_instances as $type){
                echo $this->Html->link($type->work_type->nome.' - ', ['controller' => 'WorkTypes', 'action' => 'view', $type->id]);
                    } ?>
                </td>
        </tr>
        <tr>
            <th><?= __('Agent') ?></th>
            <td><?= $contract->has('agent') ? $this->Html->link($contract->agent->nome, ['controller' => 'Agents', 'action' => 'view', $contract->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Client') ?></th>
            <td><?= $contract->has('client') ? $this->Html->link($contract->client->nome, ['controller' => 'Clients', 'action' => 'view', $contract->client->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Importo') ?></th>
            <td>€ <?= $this->Number->format($contract->importo) ?></td>
        </tr>
        <tr>
            <th><?= __('Provvigione (in %)') ?></th>
            <td><?= h($contract->provvigione) ?>%</td>
        </tr>
        <tr>
            <th><?= __('Bonus') ?></th>
            <td><?= $this->Number->format($contract->bonus) ?></td>
        </tr>
        <tr>
            <th><?= __('Data contratto') ?></th>
            <td><?= date_format($contract->data_contratto, 'd-m-Y') ?></td>
        </tr>
    </table>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                            Riepilogo
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <!--<li class="active"><a href="#home" data-toggle="tab">Totale contratto</a>
                        </li>-->
                        <?php 
                        $primo = 1;
                            foreach($contract->work_type_instances as $type){
                                
                                if($primo){
                                    $classe='class="active" ';
                                    $aria_expanded='aria-expanded=true';
                                    $primo=0;
                                } else {
                                    $classe='';
                                    $aria_expanded='';
                                }
                                echo '<li '.$classe.'><a href="#type'.$type->id.'" data-toggle="tab" '.$aria_expanded.'>'.$type->work_type->nome.'</a>
                                      </li>';
                            }
                        ?>
                    </ul>

                <!-- Tab panes -->
                    <div class="tab-content">
                        <!--<div class="tab-pane fade in active" id="home">
                            <h4>Riepilogo contratto</h4>
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-pie-chart"></div>
                            </div>
                        </div> -->
                        
                        <?php 
                        $primo = 1;
                            foreach($contract->work_type_instances as $type){
                                $payed=0;
                                $notPayed=0;
                                $delayed=0;
                                $provvigioniPayed=0;
                                $provvigioniNotPayed=0;
                                
                                if($primo){
                                    $class='tab-pane fade in active';
                                    $primo=0;
                                } else {
                                    $class='tab-pane fade';
                                }
                                echo '<div class="'.$class.'" id="type'.$type->id.'">
                                    <div class="row">
                                    <div class="col-lg-12">
                                    <div class="alert alert-danger alert-dismissable" id="ritardo'.$type->id.'" style="display: none">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        Questa istanza presenta pagamenti scaduti non ancora saldati!
                                    </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                     <div class="col-lg-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Pagamenti
                                                </div>
                                                <!-- .panel-heading -->
                                        <div class="panel-body">';
                                            //<div class="panel-group" id="accordion'.$type->id.'">';
                                echo '<div class="table-responsive">
                                                            <table class="table ">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Fatt</th>
                                                                        <th>Importo</th>
                                                                        <th>Scadenza</th>
                                                                        <th>Pagamento</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';
                                            foreach($type->payments as $pay){
                                                $panelType = "default";
                                                if($pay->payment_type_id!=3){
                                                    if($pay->payed){
                                                        $payed=$payed+$pay->importo;
                                                        $panelType = "success";
                                                    }
                                                    else{
                                                        $now = new DateTime();
                                                        $date = new DateTime($pay->data_scadenza);
                                                        if($date<$now){
                                                            $delayed=$delayed+$pay->importo;
                                                            $panelType = "danger";
                                                        }
                                                        else{
                                                            $notPayed = $notPayed+$pay->importo;
                                                            $panelType = "warning";
                                                        }
                                                    }
                                                /*echo '<div class="panel panel-'.$panelType.'">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion'.$type->id.'" href="#pay'.$pay->id.'" aria-expanded="true">ID: '.$pay->id.'</a>
                                                        </h4>
                                                    </div>
                                                    <div id="pay'.$pay->id.'" class="panel-collapse collapse in">*/
                                                        
                                                          echo      '
                                                                    <tr class="row-'.$panelType.'">
                                                                        <td>'.$pay->numero_fattura.'</td>
                                                                        <td>€ '.$pay->importo.'</td>';
                                                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'd-m-Y');
                                                                        echo '<td>'.$data_scadenza.'</td>';
                                                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                                                                        echo '<td>'.$data_pagamento.'</td>
                                                                    </tr>
                                                                
                                                            ';
                                                }
                                            }
                                        echo    '</tbody></table>
                                                        </div>
                                                        <!-- /.table-responsive --></div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                        </div>
                                        
                                        <div class="col-lg-6">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Provvigioni - '; echo $this->Html->link($contract->agent->nome, ['controller' => 'Agents', 'action' => 'view', $contract->agent->id]);
                                                echo '</div>
                                                <!-- .panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                            <table class="table ">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Importo</th>
                                                                        <th>Scadenza</th>
                                                                        <th>Pagamento</th>
                                                                        <th>Pagato</th>
                                                                        <th>Fatt</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';
                                            foreach($type->payments as $pay){
                                                $panelType = "default";
                                                if($pay->payment_type_id===3){
                                                    if($pay->payed){
                                                        $provvigioniPayed=$provvigioniPayed+$pay->importo;
                                                        $panelType = "success";
                                                    }
                                                    else{
                                                        /*$now = new DateTime();
                                                        $date = new DateTime($pay->data_scadenza);
                                                        if($date<$now){
                                                            $delayed=$delayed+$pay->importo;
                                                            $panelType = "danger";
                                                        }
                                                        else{*/
                                                            $provvigioniNotPayed = $provvigioniNotPayed+$pay->importo;
                                                            $panelType = "warning";
                                                        //}
                                                    }
                                                echo '
                                                                    <tr class="row-'.$panelType.'">
                                                                        <td>€ '.$pay->importo.'</td>';
                                                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'm-Y');
                                                                        echo '<td>'.$data_scadenza.'</td>';
                                                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'm-Y');
                                                                        echo '<td id="pay'.$pay->id.'scadenza">'.$data_pagamento.'</td>
                                                                        <td><div class="col-lg-6" id="pay'.$pay->id.'payed">';
                                                                            if($pay->payed){
                                                                                echo '<span class="glyphicon glyphicon-ok"></span></div>
                                                                                    <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'" checked></input></div>';
                                                                            } else {
                                                                                echo '<span class="glyphicon glyphicon-remove"></span></div>
                                                                                <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                                                                          
                                                                            }
                                                            echo '</td>
                                                                <td>';
                                                                echo $this->Form->input($pay->id, ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                'id' => 'pay'.$pay->id.'fat', 'style'=>'width: 50px', 'class'=>'num_ft2']);
                                                            
                                                            echo '</td>
                                                                    </tr>';
                                                }
                                            }
                                        echo    '</tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                        </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                        
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <tr>
                                                            <td>Tipo di pagamento</td>
                                                            <td>'.$type->payment_type->nome.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Anno inizio</td>
                                                            <td>'.$type->data_inizio.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td>'.$type->status->nome.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Saldato</td>';
                                                            if($type->saldato){
                                                                echo '<td><span class="glyphicon glyphicon-ok"></span></td>';
                                                            } else {
                                                                echo '<td><span class="glyphicon glyphicon-remove"></span></td>';
                                                            }
                                                        echo '</tr>
                                                        <tr>
                                                            <td>Importo</td>
                                                            <td>€ '.$type->importo.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Descrizione</td>
                                                            <td>'.$type->descrizione.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Pagato</td>
                                                            <td>€ '.$payed.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Da pagare</td>';
                                                            $notPayedT = $notPayed + $delayed;
                                                    echo   '<td>€ '.$notPayedT.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>In ritardo</td>
                                                            <td>€ '.$delayed.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Provvigione pagata</td>
                                                            <td>€ '.$provvigioniPayed.'</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Provvigione non pagata</td>
                                                            <td>€ '.$provvigioniNotPayed.'</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>

                                    </div>';
                                    /*<div class="row">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="chart'.$type->id.'"></div>
                                    </div>
                                    </div>*/
                                    echo '</div>';
                            }
                        ?>
                        
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
        
 <?php
    foreach($contract->work_type_instances as $type){
        $payed=0;
        $notPayed=0;
        $delayed=0;
        
        foreach($type->payments as $pay){
            if($pay->payment_type_id!=3){
                if($pay->payed){
                    $payed=$payed+$pay->importo;
                }
                else{
                    $now = new DateTime();
                    $date = new DateTime($pay->data_scadenza);
                    if($date<$now){
                        $delayed=$delayed+$pay->importo;
                    }
                    else{
                        $notPayed = $notPayed+$pay->importo;
                    }
                }
            }
            
        }
        $this->Html->scriptStart(['block' => 'scriptBottom']);
        /*echo 'var data'.$type->id.' = [
            { label: "Pagamenti effettuati",  data: "'.$payed.'", color: "#80009B"},
            { label: "Pagamenti da effettuare",  data: "'.$notPayed.'", color: "#4572A7"},
            { label: "Pagamenti in ritardo",  data: "'.$delayed.'", color: "#4500A7"},
            ];
 
            $(document).ready(function () {
            $.plot($("#chart'.$type->id.'"), data'.$type->id.', {
                series: {
                    pie: {
                        show: true,
                        radius: 1
                    }
                },
                legend: {
                    show: false
                }
            });*/
            echo 'if('.$delayed.'>0){
                $("#ritardo'.$type->id.'").show();
                $("#scaduti").show();    
            };
            
        ';
        $this->Html->scriptEnd();
    }
    
    echo $this->fetch('scriptBottom');           
?>

        