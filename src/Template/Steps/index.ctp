
<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="steps index large-9 medium-8 columns content">
    <h3><?= __('Steps') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('contract_id') ?></th>
                <th><?= $this->Paginator->sort('stepType_id') ?></th>
                <th><?= $this->Paginator->sort('importo') ?></th>
                <th><?= $this->Paginator->sort('provvigione_agent') ?></th>
                <th><?= $this->Paginator->sort('dati') ?></th>
                <th><?= $this->Paginator->sort('inizio_step') ?></th>
                <th><?= $this->Paginator->sort('fine_step') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($steps as $step): ?>
            <tr>
                <td><?= $this->Number->format($step->id) ?></td>
                <td><?= $step->has('contract') ? $this->Html->link($step->contract->client->nome.$step->contract->id, ['controller' => 'Contracts', 'action' => 'view', $step->contract->id]) : '' ?></td>
                <td><?= $step->has('step_type') ? $this->Html->link($step->step_type->nome, ['controller' => 'StepTypes', 'action' => 'view', $step->step_type->id]) : '' ?></td>
                <td>€ <?= $this->Number->format($step->importo) ?></td>
                <td><?= $this->Number->format($step->provvigione_agent) ?>%</td>
                <td><?= h($step->dati) ?></td>
                <td><?= h($step->inizio_step) ?></td>
                <td><?= h($step->fine_step) ?></td>
                <td><?= h($step->modified) ?></td>
                <td><?= h($step->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $step->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $step->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $step->id], ['confirm' => __('Are you sure you want to delete # {0}?', $step->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
