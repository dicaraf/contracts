<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Step'), ['action' => 'edit', $step->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Step'), ['action' => 'delete', $step->id], ['confirm' => __('Are you sure you want to delete # {0}?', $step->id)]) ?> </li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="steps view large-9 medium-8 columns content">
    <h3><?= h($step->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Contract') ?></th>
            <td><?= $step->has('contract') ? $this->Html->link($step->contract->client->nome.$step->contract->id, ['controller' => 'Contracts', 'action' => 'view', $step->contract->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Step Type') ?></th>
            <td><?= $step->has('step_type') ? $this->Html->link($step->step_type->nome, ['controller' => 'StepTypes', 'action' => 'view', $step->step_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Dati') ?></th>
            <td><?= h($step->dati) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($step->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Importo') ?></th>
            <td>€ <?= $this->Number->format($step->importo) ?></td>
        </tr>
        <tr>
            <th><?= __('Provvigione Agent') ?></th>
            <td><?= $this->Number->format($step->provvigione_agent) ?>%</td>
        </tr>
        <tr>
            <th><?= __('Inizio Step') ?></th>
            <td><?= h($step->inizio_step) ?></td>
        </tr>
        <tr>
            <th><?= __('Fine Step') ?></th>
            <td><?= h($step->fine_step) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($step->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($step->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Payments') ?></h4>
        <?php if (!empty($step->payments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('InEntrata') ?></th>
                <th><?= __('Scadenza') ?></th>
                <th><?= __('Importo') ?></th>
                <th><?= __('Fatturato') ?></th>
                <th><?= __('Payed') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($step->payments as $payments): ?>
            <tr>
                <td><?= h($payments->id) ?></td>
                <td><?= h($payments->inEntrata) ?></td>
                <td><?= h($payments->date) ?></td>
                <td>€ <?= h($payments->importo) ?></td>
                <td><?= h($payments->fatturato) ?></td>
                <td><?= h($payments->payed) ?></td>
                <td><?= h($payments->created) ?></td>
                <td><?= h($payments->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Payments', 'action' => 'view', $payments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Payments', 'action' => 'edit', $payments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Payments', 'action' => 'delete', $payments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $payments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
