<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $step->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $step->id)]
            )
        ?></li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="steps form large-9 medium-8 columns content">
    <?= $this->Form->create($step) ?>
    <fieldset>
        <legend><?= __('Edit Step') ?></legend>
        <?php
            echo $this->Form->input('contract_id', ['options' => $contracts]);
            echo $this->Form->input('stepType_id', ['options' => $stepTypes]);
            echo $this->Form->label('importo', 'Importo percentuale su totale (tra 0 e 100)');
            echo $this->Form->input('importo', ['label' => false]);
            echo $this->Form->label('provvigione_agent', 'Percentuale provvigione agente (tra 0 e 100)');
            echo $this->Form->input('provvigione_agent', ['label' => false]);
            echo $this->Form->input('dati');
            echo $this->Form->input('inizio_step');
            echo $this->Form->input('fine_step');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
