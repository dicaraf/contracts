<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Contracts';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= ''//$this->Html->meta('icon') ?>
    <?php
    $this->Html->script(['jquery.min.js'], ['block' => true]);
    $this->Html->css(['bootstrap.min.css'], ['block' => true]);
    $this->Html->css(['sb-admin-2.css'], ['block' => true]);
    $this->Html->css(['metisMenu.min.css'], ['block' => true]);
    $this->Html->css(['font-awesome.min.css'], ['block' => true]);
    $this->Html->css(['bootstrap-datetimepicker.min.css'], ['block' => true]);
    $this->Html->script(['bootstrap.min.js'], ['block' => true]);
    $this->Html->script(['sb-admin-2.js'], ['block' => true]);
    $this->Html->script(['metisMenu.min.js'], ['block' => true]);
    $this->Html->script(['script.js'], ['block' => true]);
    $this->Html->script(['jquery.flot.js'], ['block' => true]);
$this->Html->script(['jquery.flot.pie.js'], ['block' => true]);
$this->Html->script(['jquery.flot.time.js'], ['block' => true]);
$this->Html->script(['jquery.flot.resize.js'], ['block' => true]);
$this->Html->script(['bootstrap-datetimepicker.min.js'], ['block' => true]);
$this->Html->script(['jquery-ui.min.js'], ['block' => true]);
$this->Html->css(['jquery-ui.min.css'], ['block' => true]);
$this->Html->script(['jquery.dataTables.min.js'], ['block' => true]);
$this->Html->css(['jquery.dataTables.min.css'], ['block' => true]);

    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div id="wait-ajax" style="position:fixed;z-index:100;width:100%;height:100%;opacity: .5;background-color: white; visibility: hidden"><?= $this->Html->image('wait.gif', ['alt' => 'Wait', 'style'=>'position: relative; top: 30%; left: 40%']);?></div>
    <div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="" class="navbar-brand"><?= $this->fetch('title') ?></a>
        </div>    
            <ul class="nav navbar-top-links navbar-right">
                <li></li>
                <li></li>
            </ul>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    
                    <?php if ($this->element('menu')){
                            echo $this->element('menu');
                        }
                    ?>
                </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div id="page-wrapper">
            <?= $this->fetch('content') ?>

    
    <footer>
    </footer>
    </div>
    </div>
</body>
</html>
