<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi nuovo ordine</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento dati ordine
            </div>
            <div class="panel-body">
    <?= $this->Form->create($order, []) ?>
    <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('numero') ?>
                <?= $this->Form->input('agent_id', ['options' => $agents, 'default' => 1, 'empty' => true]) ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('status_id', ['options' => $statuses]); ?>
                <div class="row">
                <div class="col-lg-6"><?= $this->Form->input('provvigione', ['placeholder' => 'in % (0-100)']) ?></div>
                <div class="col-lg-6"><?= $this->Form->input('bonus', ['placeholder' => '€']) ?></div>
                </div>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('client_id', ['options' => $clients, 'empty' => true])?>
                <div class="form-group">
                    <label for="data_ordine">Data</label>
                    <div class="input-group date data_normale">
                        <input type="text" class="form-control" name="data_ordine" id="data_ordine" readonly/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-12"><?php    
            echo $this->Form->input('note', []);
            echo $this->Form->label('worktype_str', 'Inserisci una stringa per cercare i tipi di lavoro');
            echo $this->Form->input('worktype_str', ['label' => false]); 
            /*echo $this->Form->input('work_types._ids', ['options' => $workTypes]);*/
            $counter = 0;
            ?>
            <div id='worktype_result'></div>
            
            </div>
        </div>
        <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tipi di lavoro
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
        
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-default']) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>      
<p id="counter" style="visibility: hidden"><?= $counter ?></p>
<?php 
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo '$(function(){
    $("#client-id").combobox();
});
(function ($) {
        $.widget("ui.combobox", {
            _create: function () {
                var input,
                  that = this,
                  wasOpen = false,
                  select = this.element.hide(),
                  selected = select.children(":selected"),
                  defaultValue = selected.text() || "",
                  wrapper = this.wrapper = $("<span>")
                    .addClass("ui-combobox")
                    .insertAfter(select);

                function removeIfInvalid(element) {
                    var value = $(element).val(),
                      matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                      valid = false;
                    select.children("option").each(function () {
                        if ($(this).text().match(matcher)) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    if (!valid) {
                        
                        $(element).val(defaultValue);
                        select.val(defaultValue);
                        input.data("ui-autocomplete").term = "";
                    }
                }

                input = $("<input>")
                  .appendTo(wrapper)
                  .val(defaultValue)
                  .attr("title", "")
                  .addClass("ui-state-default ui-combobox-input")
                  .width(select.width())
                  .autocomplete({
                      delay: 0,
                      minLength: 0,
                      autoFocus: true,
                      source: function (request, response) {
                          var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                          response(select.children("option").map(function () {
                              var text = $(this).text();
                              if (this.value && (!request.term || matcher.test(text)))
                                  return {
                                      label: text.replace(
                                        new RegExp(
                                          "(?![^&;]+;)(?!<[^<>]*)(" +
                                          $.ui.autocomplete.escapeRegex(request.term) +
                                          ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                        ), "<strong>$1</strong>"),
                                      value: text,
                                      option: this
                                  };
                          }));
                      },
                      select: function (event, ui) {
                          ui.item.option.selected = true;
                          that._trigger("selected", event, {
                              item: ui.item.option
                          });
                      },
                      change: function (event, ui) {
                          if (!ui.item) {
                              removeIfInvalid(this);
                          }
                      }
                  })
                  .addClass("ui-widget ui-widget-content ui-corner-left");

                input.data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                      .append("<a>" + item.label + "</a>")
                      .appendTo(ul);
                };

                $("<a>")
                  .attr("tabIndex", -1)
                  .appendTo(wrapper)
                  .button({
                      icons: {
                          primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                  })
                  .removeClass("ui-corner-all")
                  .addClass("ui-corner-right ui-combobox-toggle")
                  .mousedown(function () {
                      wasOpen = input.autocomplete("widget").is(":visible");
                  })
                  .click(function () {
                      input.focus();

                      // close if already visible
                      if (wasOpen) {
                          return;
                      }

                      // pass empty string as value to search for, displaying all results
                      input.autocomplete("search", "");
                  });
            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });
    })(jQuery);';
  $this->Html->scriptEnd();
  echo $this->fetch('scriptBottom');
  ?>