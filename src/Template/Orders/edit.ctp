<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modifica Ordine</h1>
        <h4> <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $order->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]
            )
        ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Modifica dati ordine
            </div>
            <div class="panel-body">
    <?= $this->Form->create($order) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('numero') ?>
                <?= $this->Form->input('agent_id', ['options' => $agents, 'empty' => true]) ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('status_id', ['options' => $statuses]); ?>
                <div class="row">
                <div class="col-lg-6"><?= $this->Form->input('provvigione', ['placeholder' => 'in % (0-100)']) ?></div>
                <div class="col-lg-6"><?= $this->Form->input('bonus', ['placeholder' => '€']) ?></div>
                </div>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('client_id', ['options' => $clients, 'empty' => true])?>
                <div class="form-group">
                    <label for="data_ordine">Data</label>
                    <div class="input-group date data_normale">
                        <?php (!isset($order->data_ordine)) ? $data_ordine= '': $data_ordine = date_format($order->data_ordine, 'd-m-Y')?>
                        <input type="text" class="form-control" name="data_ordine" id="data_ordine" value="<?php echo $data_ordine?>" readonly/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-12"><?php    
            echo $this->Form->input('note', []);
            echo $this->Form->label('worktype_str', 'Inserisci una stringa per cercare i tipi di lavoro');
            echo $this->Form->input('worktype_str', ['label' => false]); 
            /*echo $this->Form->input('work_types._ids', ['options' => $workTypes]);*/
            $counter = 0;
            ?>
                
            <div id='worktype_result'></div>
            </div>
            </div>
        <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tipi di lavoro
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
            <?php
            foreach($order->work_type_instances as $type){
                
               echo '<div class="panel panel-default" id="contractworktype'.$counter.'"><fieldset>
                    <div class="panel-heading">
                        <div class="row">
                        <div class="col-lg-8">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$counter.'">Tipo di lavoro: '.$type->work_type->nome.'</a>
                        </h4>
                        </div>
                        <div class="col-lg-4">
                            <p class="pull-right" id="removeWorkType" name="'.$type['id'].'">Remove</p>
                        </div>
                        </div>
                    </div>
                 <div id="collapse'.$counter.'" class="panel-collapse collapse">   
               <div class="panel-body">
            <div class="col-lg-4">';
               echo $this->Form->hidden('work_type_instances.'.$counter.'.id', ['value' => $type['id']]);
               echo $this->Form->hidden('work_type_instances.'.$counter.'.work_type_id', ['value' => $type->work_type_id]);
                echo $this->Form->input('work_type_instances.'.$counter.'.status_id', ['value' => $type->status_id, 'options' => $statuses]);
            echo $this->Form->input('work_type_instances.'.$counter.'.saldato', ['type' => 'checkbox', 'default' => $type->saldato, 'disabled' => true]);   
                echo '</div>
            <div class="col-lg-4">';
                
                echo $this->Form->input('work_type_instances.'.$counter.'.importo', ['default' => $type->importo, 'placeholder' => '€']);     
                
                echo $this->Form->input('work_type_instances.'.$counter.'.payment_type_id', ['value' => $type->payment_type_id, 'options' => $paymentTypes]);
                 
                echo '</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counter.'-data_inizio">Data inizio</label>
                    <div class="input-group date data_inizio">
                        <input type="text" class="form-control" name="work_type_instances['.$counter.'][data_inizio]" value="'.$type->data_inizio.'" readonly />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>                        
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
                echo $this->Form->input('work_type_instances.'.$counter.'.descrizione', ['default' => $type->descrizione]);
                echo '</div>'
                . '</div>
                       <div class="row">
            <div class="col-lg-12"> 
            <div class="panel panel-default">
                        <div class="panel-heading">
                            Pagamenti per questa instanza
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">';
                $counterPay = 0;
                $counterStampati = 0;
                            foreach($type->payments as $pay){
                                if($pay->payment_type_id===3){
                                    //echo '<li><a href="#'.$counter.'pay'.$counterPay.'" data-toggle="tab">Provvigione</a> </li>';
                                } 
                                elseif($pay->payment_type_id===1) {  //provvigione
                                    if($counterPay===0){
                                        echo '<li class="active" id="'.$counter.'pay'.$counterPay.'li"><a href="#'.$counter.'pay'.$counterPay.'" data-toggle="tab" >Ricorr '.$pay->data_scadenza->year.'</a> </li>';
                                    }
                                    else{
                                        echo '<li id="'.$counter.'pay'.$counterPay.'li"><a href="#'.$counter.'pay'.$counterPay.'" data-toggle="tab">Ricorr '.$pay->data_scadenza->year.'</a> </li>';
                                    }
                                }
                                else {
                                    if($counterPay===0){
                                        $counterStampati++;
                                        echo '<li class="active" id="'.$counter.'pay'.$counterPay.'li"><a href="#'.$counter.'pay'.$counterPay.'" data-toggle="tab" >Rata '.$counterStampati.'</a> </li>';
                                    }
                                    else{
                                        $counterStampati++;
                                        echo '<li id="'.$counter.'pay'.$counterPay.'li"><a href="#'.$counter.'pay'.$counterPay.'" data-toggle="tab">Rata '.$counterStampati.'</a> </li>';
                                    }
                                }
                                $counterPay++;
                                
                            }
                            echo '<li class="addPay" name="'.$counter.'" value="'.$counterPay.'"><a>+</a></li>';
                            echo '</ul>

                            <!-- Tab panes -->
                            <div class="tab-content" id="tab-content'.$counter.'">';
                            $counterPay = 0;
                            foreach($type->payments as $pay){
                              if($pay->payment_type_id!=3){
                                if($counterPay===0){
                                    echo '<div class="tab-pane fade in active" id="'.$counter.'pay'.$counterPay.'">
                                    <fieldset>';
                                }
                                else{
                                    echo '<div class="tab-pane fade" id="'.$counter.'pay'.$counterPay.'">'
                                            . '<fieldset>';
                                }
                                echo '<div class="row">
            <div class="col-lg-4">';
            
            echo $this->Form->input('work_type_instances.'.$counter.'.payments.'.$counterPay.'.importo', ['default' => $pay->importo, 'placeholder' => '€']);
            echo $this->Form->input('work_type_instances.'.$counter.'.payments.'.$counterPay.'.payed', ['default' => $pay->payed]);
            
            echo '</div>
            
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counter.'-payments-'.$counterPay.'-data_scadenza">Data scadenza</label>
                    <div class="input-group date data_normale">
                        
                        <input type="text" class="form-control" name="work_type_instances['.$counter.'][payments]['.$counterPay.'][data_scadenza]" value="'.date_format($pay->data_scadenza, 'd-m-Y').'" readonly/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
                echo $this->Form->label('work_type_instances.'.$counter.'.payments.'.$counterPay.'.numero_fattura', 'Numero fattura');
                echo $this->Form->input('work_type_instances.'.$counter.'.payments.'.$counterPay.'.numero_fattura', ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                                                    'id' => 'work-type-instances-'.$counter.'-payments-'.$counterPay.'-payedfat']);
            
                echo $this->Form->hidden('work_type_instances.'.$counter.'.payments.'.$counterPay.'.id', ['value' => $pay->id]);
                echo $this->Form->hidden('work_type_instances.'.$counter.'.payments.'.$counterPay.'.payment_type_id', ['value' => $pay->payment_type_id]);
            echo '</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counter.'-payments-'.$counterPay.'-data_pagamento">Data pagamento</label>
                    <div class="input-group date data_normale">';
                    if($pay->payed){
                            echo '<input type="text" class="form-control" name="work_type_instances['.$counter.'][payments]['.$counterPay.'][data_pagamento]" id="work-type-instances-'.$counter.'-payments-'.$counterPay.'-payedday" value="'.date_format($pay->data_pagamento, 'd-m-Y').'" readonly/>';
                        } else{
                            echo '<input type="text" class="form-control" name="work_type_instances['.$counter.'][payments]['.$counterPay.'][data_pagamento]" id="work-type-instances-'.$counter.'-payments-'.$counterPay.'-payedday" readonly disabled/>';
                        }
                        echo '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
            if($pay->payment_type_id === 1 && !$pay->payed){
                echo '<div class="col-lg-4"><button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se questo pagamento viene segnato come pagato scegliere la data di scadenza del prossimo pagamento ricorrente, se vuoto non verrà inserito il nuovo pagamento."><span class="glyphicon glyphicon-info-sign"></span></button></div>';
                echo '<div class="col-lg-8">';
                echo $this->Form->input('work_type_instances.'.$counter.'.payments.'.$counterPay.'.ricorrenza', ['style'=>'visibility:hidden', 'options' => $mesi, 'empty' => true, 'default' => $type->payment_type->ricorrenza,
                                                                                                                    'id' => 'work-type-instances-'.$counter.'-payments-'.$counterPay.'-payedric', 'label' => false]);
                echo '</div>';
            }
                //echo $this->Form->postLink(__('Elimina pagamento'), ['controller' => 'Payments','action' => 'delete', $pay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pay->id)]);
               echo '<p class="removePayment" value='.$pay->id.' name="'.$counter.'pay'.$counterPay.'"> Delete pagamento <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></p>';
            echo '</div></div></fieldset></div>';
                                
                                
                              }
                              $counterPay++;
                            }
                                
                            echo '</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                        </div></div></div></fieldset></div>';
                $counter++;
            }
            
            //echo $this->Form->input('work_types._ids', ['options' => $workTypes]);
            
        ?>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>  
<p id="counter" style="visibility: hidden"><?= $counter ?></p>