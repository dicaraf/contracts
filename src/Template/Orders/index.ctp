<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Ordini</h1>
    </div>
    <div class="col-lg-6">
        <?= $this->Html->link(__('Aggiungi ordine'), ['controller' => 'Orders', 'action' => 'add'], ['class'=>'page-header pull-right']) ?>
    </div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="table-responsive">
    <table id="table-index" class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Status</th>
                <th>Importo</th>
                <th>Data ordine</th>
                <th>Cliente</th>
                <th>Agente</th>
                <th>Provvigione</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $order): ?>
            <?php 
            $testo='';
            foreach ($order->work_type_instances as $type){
                $testo = $testo.$type->work_type->codice.':'.$type->importo.'€-';
            }
            ?>
            <tr>
                <td><?= h($order->numero) ?></td>
                <td><?= $order->has('status') ? $this->Html->link($order->status->nome, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                <td><div class="col-lg-6">€ <?= h($order->importo) ?></div><div class="col-lg-6"><button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="<?=$testo?>"><span class="glyphicon glyphicon-info-sign"></span></button></div></td>
                <td><?= isset($contract->data_ordine) ? date_format($contract->data_ordine, 'd-m-Y') : '' ?></td>
                <td><?= $order->has('client') ? $this->Html->link($order->client->nome, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
                <td><?= $order->has('agent') ? $this->Html->link($order->agent->nome, ['controller' => 'Agents', 'action' => 'view', $order->agent->id]) : '' ?></td>
                <td><?= $this->Number->format($order->provvigione) ?>%</td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $order->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $order->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
</div>
</div>
</div>  
