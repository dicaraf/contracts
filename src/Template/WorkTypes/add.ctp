<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi tipo lavoro</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento dati 
            </div>
            <div class="panel-body">    <?= $this->Form->create($workType) ?>
                    <?php $this->Form->templates($form_templates['fullForm']); ?>

    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('nome'); ?>
                <?= $this->Form->input('descrizione'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('codice'); ?>
                <?= $this->Form->label('durata', 'Ricorrenza di default'); ?>
                <?= $this->Form->input('durata', ['options' => $mesi, 'label' => false, 'empty' => true]); ?>            
            </div>
            <div class="col-lg-4">
                <?= $this->Form->label('p_type_default', 'Tipo pagamento di default'); ?>
                <?= $this->Form->input('p_type_default', ['options' => $payment_types, 'label' => false]); ?>          
            </div>
        </div>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div></div></div></div>
