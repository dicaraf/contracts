<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= h('Tipo lavoro '.$workType->nome) ?></h1>
        <h4><?= $this->Html->link(__('Edit '), ['action' => 'edit', $workType->id]) ?> </h4>
        <h4><?= $this->Form->postLink(__('Delete '), ['action' => 'delete', $workType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $workType->id)]) ?> </h4>
    </div>
</div>
    <div class="panel-body">

    <table class="table table-striped">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($workType->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Codice') ?></th>
            <td><?= h($workType->codice) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrizione') ?></th>
            <td><?= h($workType->descrizione) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($workType->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Durata') ?></th>
            <td><?= $this->Number->format($workType->durata) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Contracts') ?></h4>
        <?php if (!empty($workType->contracts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Status Id') ?></th>
                <th><?= __('Numero') ?></th>
                <th><?= __('Importo') ?></th>
                <th><?= __('Note') ?></th>
                <th><?= __('Agent Id') ?></th>
                <th><?= __('Client Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Provvigione') ?></th>
                <th><?= __('Bonus') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($workType->contracts as $contracts): ?>
            <tr>
                <td><?= h($contracts->id) ?></td>
                <td><?= h($contracts->status_id) ?></td>
                <td><?= h($contracts->numero) ?></td>
                <td>€ <?= h($contracts->importo) ?></td>
                <td><?= h($contracts->note) ?></td>
                <td><?= h($contracts->agent_id) ?></td>
                <td><?= h($contracts->client_id) ?></td>
                <td><?= h($contracts->created) ?></td>
                <td><?= h($contracts->modified) ?></td>
                <td><?= h($contracts->provvigione) ?>%</td>
                <td><?= h($contracts->bonus) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Contracts', 'action' => 'view', $contracts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Contracts', 'action' => 'edit', $contracts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Contracts', 'action' => 'delete', $contracts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contracts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
