<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Tipi di lavoro</h1>
    </div>
    <div class="col-lg-6">
        <?= $this->Html->link(__('Aggiungi tipo di lavoro'), ['controller' => 'WorkTypes', 'action' => 'add'], ['class'=>'page-header pull-right']) ?>
    </div>
</div>
<div class="panel-body">    
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('codice') ?></th>
                <th><?= $this->Paginator->sort('descrizione') ?></th>
                <th><?= $this->Paginator->sort('payment_type_default') ?></th>
                <th><?= $this->Paginator->sort('durata') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($workTypes as $workType): ?>
            <tr>
                <td><?= h($workType->nome) ?></td>
                <td><?= h($workType->codice) ?></td>
                <td><?= h($workType->descrizione) ?></td>
                <td><?= h($workType->payment_type->nome) ?></td>
                <td><?= $this->Number->format($workType->durata) ?> mesi</td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $workType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $workType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $workType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $workType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
