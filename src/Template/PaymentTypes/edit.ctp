<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modifica Tipo pagamento</h1>
        <h4> <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $paymentType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $paymentType->id)]
            )
        ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Modifica dati tipo pagamento
            </div>
            <div class="panel-body">
    <?= $this->Form->create($paymentType) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('nome'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('descrizione'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->label('ricorrenza', 'Ricorrenza pagamento di default'); ?>
                <?= $this->Form->input('ricorrenza', ['options' => $mesi, 'empty' => true, 'label' => false]); ?>
            </div>
                        
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div></div></div></div>
