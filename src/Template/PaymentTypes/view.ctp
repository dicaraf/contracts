<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= h('Tipo pagamento '.$paymentType->nome) ?></h1>
        <h4><?= $this->Html->link(__('Modifica tipo pagamento'), ['action' => 'edit', $paymentType->id]) ?> </h4>
        <h4><?= $this->Form->postLink(__('Delete tipo pagamento'), ['action' => 'delete', $paymentType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentType->id)]) ?> </h4>
    </div>
</div>
    <table class="table table-striped">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($paymentType->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrizione') ?></th>
            <td><?= h($paymentType->descrizione) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($paymentType->id) ?></td>
        </tr>
    </table>
</div>
