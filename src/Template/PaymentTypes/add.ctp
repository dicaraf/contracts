<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi nuovo Tipo pagamento</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento tipo pagamento
            </div>
            <div class="panel-body">
    <?= $this->Form->create($paymentType) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('nome'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('descrizione'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->label('ricorrenza', 'Ricorrenza pagamento di default'); ?>
                <?= $this->Form->input('ricorrenza', ['options' => $mesi, 'empty' => true, 'label' => false]); ?>
            </div>
                        
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
            
