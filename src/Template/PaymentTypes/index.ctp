<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Tipi pagamento</h1>
    </div>
    <div class="col-lg-6">
        <?= $this->Html->link(__('Aggiungi tipo pagamento'), ['controller' => 'PaymentTypes', 'action' => 'add'], ['class'=>'page-header pull-right']) ?>
    </div>
</div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('descrizione') ?></th>
                <th><?= $this->Paginator->sort('ricorrenza') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($paymentTypes as $paymentType): ?>
            <tr>
                <td><?= h($paymentType->nome) ?></td>
                <td><?= h($paymentType->descrizione) ?></td>
                <td><?= (isset($paymentType->ricorrenza)) ? h($paymentType->ricorrenza).'mesi' : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $paymentType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $paymentType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $paymentType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
