<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Istanze dei contratti</h1>
    </div>
    
</div>
<div class="panel-body">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('status_id') ?></th>
                <th><?= $this->Paginator->sort('contract_id') ?></th>
                <th><?= $this->Paginator->sort('work_type_id') ?></th>
                <th><?= $this->Paginator->sort('importo') ?></th>
                <th><?= $this->Paginator->sort('saldato') ?></th>
                <th><?= $this->Paginator->sort('descrizione') ?></th>
                <th><?= $this->Paginator->sort('data_inizio') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($WorkTypeInstances as $workTypeInstances): ?>
            <tr>
                <td><?= $this->Number->format($workTypeInstances->id) ?></td>
                <td><?= $workTypeInstances->has('status') ? $this->Html->link($workTypeInstances->status->id, ['controller' => 'Statuses', 'action' => 'view', $workTypeInstances->status->id]) : '' ?></td>
                <td><?= $workTypeInstances->has('contract') ? $this->Html->link($workTypeInstances->contract->id, ['controller' => 'Contracts', 'action' => 'view', $workTypeInstances->contract->id]) : '' ?></td>
                <td><?= $workTypeInstances->has('work_type') ? $this->Html->link($workTypeInstances->work_type->id, ['controller' => 'WorkTypes', 'action' => 'view', $workTypeInstances->work_type->id]) : '' ?></td>
                <td>€ <?= $this->Number->format($workTypeInstances->importo) ?></td>
                <td><?= $this->Number->format($workTypeInstances->saldato) ?></td>
                <td><?= h($workTypeInstances->descrizione) ?></td>
                <td><?= h($workTypeInstances->data_inizio) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $workTypeInstances->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $workTypeInstances->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $workTypeInstances->id], ['confirm' => __('Are you sure you want to delete # {0}?', $workTypeInstances->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
