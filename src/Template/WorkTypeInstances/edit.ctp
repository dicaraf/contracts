<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit istanza</h1>
        <h4> <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $workTypeInstances->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $workTypeInstances->id)]
            )
        ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Modifica dati contratto
            </div>
            <div class="panel-body">
    <?= $this->Form->create($workTypeInstances) ?>
                    <?php $this->Form->templates($form_templates['fullForm']); ?>

    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('work_type_id', ['options' => $workTypes]); ?>
                <?= $this->Form->input('contract_id', ['options' => $contracts]);?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('status_id', ['options' => $statuses]); ?>
                <?= $this->Form->input('importo'); ?>            
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('saldato'); ?>
                <?= $this->Form->input('data_inizio', ['empty' => true]); ?>
            </div>
            <div class="col-lg-12">
                <?= $this->Form->input('descrizione'); ?>
            </div>
            
        </div>

        
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div></div></div></div>
