
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= h('Istanza ') ?></h1>
        <h4><?= $this->Html->link(__('Edit '), ['action' => 'edit', $workTypeInstance->id]) ?> </h4>
        <h4><?= $this->Form->postLink(__('Delete '), ['action' => 'delete', $workTypeInstance->id], ['confirm' => __('Are you sure you want to delete # {0}?', $workTypeInstance->id)]) ?> </h4>
    </div>
</div>
    <div class="panel-body">

    <table class="table table-striped">
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $workTypeInstance->has('status') ? $this->Html->link($workTypeInstance->status->id, ['controller' => 'Statuses', 'action' => 'view', $workTypeInstance->status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Contract') ?></th>
            <td><?= $workTypeInstance->has('contract') ? $this->Html->link($workTypeInstance->contract->id, ['controller' => 'Contracts', 'action' => 'view', $workTypeInstance->contract->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Work Type') ?></th>
            <td><?= $workTypeInstance->has('work_type') ? $this->Html->link($workTypeInstance->work_type->id, ['controller' => 'WorkTypes', 'action' => 'view', $workTypeInstance->work_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Descrizione') ?></th>
            <td><?= h($workTypeInstance->descrizione) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($workTypeInstance->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Importo') ?></th>
            <td>€ <?= $this->Number->format($workTypeInstance->importo) ?></td>
        </tr>
        <tr>
            <th><?= __('Saldato') ?></th>
            <td><?= $this->Number->format($workTypeInstance->saldato) ?></td>
        </tr>
        <tr>
            <th><?= __('Data Inizio') ?></th>
            <td><?= h($workTypeInstance->data_inizio) ?></td>
        </tr>
    </table>
</div>
