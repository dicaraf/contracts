
<fieldset>
    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$counter?>"><?= __('Aggiungi tipo di lavoro: '.$workType[0]->nome) ?></a>
                            <p class="pull-right" id="removeWorkType">Remove</p>
                        </h4>
                    </div>

                 <div id="collapse<?=$counter?>" class="panel-collapse collapse in"> 

    
    <div class="row">
            <div class="col-lg-4">
                
                <?php
                $this->Form->templates($form_templates['fullForm']);
                echo $this->Form->hidden('work_type_instances.'.$counter.'.work_type_id', ['value' => $workTypeId]);
                echo $this->Form->input('work_type_instances.'.$counter.'.status_id', ['options' => $statuses]);
                echo $this->Form->input('work_type_instances.'.$counter.'.saldato', ['type' => 'checkbox', 'disabled' => true]);   
                echo '</div>
            <div class="col-lg-4">';
                
                echo $this->Form->input('work_type_instances.'.$counter.'.importo', ['class' => 'workTypeImporto', 'placeholder' => '€']);
                echo $this->Form->input('work_type_instances.'.$counter.'.payment_type_id', ['options' => $paymentTypes, 'default' => $workType[0]->p_type_default]);
            echo '</div>
            <div class="col-lg-4">
            <div class="form-group">
                    <label for="work-type-instances-'.$counter.'-data_inizio">Data</label>
                    <div class="input-group date data_inizio">
                        <input type="text" class="form-control" name="work_type_instances['.$counter.'][data_inizio]"  readonly/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
                echo $this->Form->input('work_type_instances.'.$counter.'.descrizione');
                echo '</div>';

    ?>
            </div>
<div class="row">
            <div class="col-lg-12"> 
            <div class="panel panel-default">
                        <div class="panel-heading">
                            Pagamenti per questa instanza
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                            <?php 
                            if($workType[0]->p_type_default===1){
                                echo    '<li class="active" id="'.$counter.'pay0li"><a href="#'.$counter.'pay0" data-toggle="tab">Ricorrente</a></li>';
                            } else {
                                echo    '<li class="active" id="'.$counter.'pay0li"><a href="#'.$counter.'pay0" data-toggle="tab">Rata</a></li>';
                            }
                            echo    '<li class="addPay" name="'.$counter.'" value="1"><a>+</a></li>'; ?>
                            </ul>
                            <!-- Tab panes -->
                <?php echo '<div class="tab-content" id="tab-content'.$counter.'">
                                <div class="tab-pane fade in active" id="'.$counter.'pay0">
                                    <fieldset>';

                                echo '<div class="row">
            <div class="col-lg-4">';

            echo $this->Form->input('work_type_instances.'.$counter.'.payments.0.importo', ['placeholder' => '€']);
            echo $this->Form->input('work_type_instances.'.$counter.'.payments.0.payed', ['type' => 'checkbox']);
            echo '</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counter.'-payments-0-data_scadenza">Data scadenza</label>
                    <div class="input-group date data_normale">
                        <input type="text" class="form-control" required="required" name="work_type_instances['.$counter.'][payments][0][data_scadenza]"  readonly/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
            echo $this->Form->label('work_type_instances.'.$counter.'.payments.0.numero_fattura', 'Numero fattura');
                echo $this->Form->input('work_type_instances.'.$counter.'.payments.0.numero_fattura', ['empty' => true, 'label' => false,
                                                                                                                    'id' => 'work-type-instances-'.$counter.'-payments-0-payedfat']);
                echo $this->Form->hidden('work_type_instances.'.$counter.'.payments.0.payment_type_id', ['value' => 0]);  
            echo '</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="work-type-instances-'.$counter.'-payments-0-data_pagamento">Data pagamento</label>
                    <div class="input-group date data_normale">
                        <input type="text" class="form-control" name="work_type_instances['.$counter.'][payments][0][data_pagamento]" id="work-type-instances-'.$counter.'-payments-0-payedday" readonly disabled/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>';
            if($workType[0]->p_type_default === 1){
                echo '<div class="col-lg-4"><button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se questo pagamento viene segnato come pagato scegliere la data di scadenza del prossimo pagamento ricorrente, se vuoto non verrà inserito il nuovo pagamento."><span class="glyphicon glyphicon-info-sign"></span></button></div>';
                echo '<div class="col-lg-8">';
                echo $this->Form->input('work_type_instances.'.$counter.'.payments.0.ricorrenza', ['style'=>'visibility:hidden', 'options' => $mesi, 'empty' => true, 'default' => $workType[0]->durata,
                                                                                                                    'id' => 'work-type-instances-'.$counter.'-payments-0-payedric', 'label' => false]);
                echo '</div>';
            }
      echo '<p class="removePayment"  name="'.$counter.'pay0"> Delete pagamento <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></p>';
            echo '</div></div></fieldset></div>';
            ?>                    
                      </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                        </div></div></div></fieldset></div>
            
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
</div></div>

