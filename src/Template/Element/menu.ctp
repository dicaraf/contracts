<li>
    <a href="http://test.librasoftsnc.it/contracts/"> Dashboard</a>
</li>
<li><?= $this->Html->link(__('Agenti'), ['controller' => 'Agents', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Clienti'), ['controller' => 'Clients', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Contratti'), ['controller' => 'Contracts', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Ordini'), ['controller' => 'Orders', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Pagamenti'), ['controller' => 'payments', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Tipologie di lavoro'), ['controller' => 'work-types', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Tipologie di pagamento'), ['controller' => 'payment-types', 'action' => 'index']) ?>
</li>
<li><?= $this->Html->link(__('Status'), ['controller' => 'Statuses', 'action' => 'index']) ?>
</li>
