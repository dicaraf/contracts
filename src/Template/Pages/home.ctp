<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Pagamenti fatture emesse (scaduti e in scadenza a <?= $mesi[$now->month-1].' '.$now->year?>)
            </div>
            <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                                    <table class="table" id="table-index3">
                                        <thead>
                                            <tr>
                                                <th>Contr/Ord</th>
                                                <th>Agente</th>
                                                <th>Cliente</th>
                                                <th>Importo</th>
                                                <th>Scadenza</th>
                                                <th>Pagamento</th>
                                                <th>Num FT</th>
                                                <th>Pagato</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                <?php
                if(isset($payScaduti[0])){
                                foreach ($payScaduti as $pay){
                                    (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'd-m-Y');
                                    (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                                    echo '<tr class="row-danger">
                                            <td><a href="contracts/view/'.$pay->contract_id.'">'.$pay->contract_name.'</a></td>
                                            <td><a href="agents/view/'.$pay->agent_id.'">'.$pay->agent_name.'</a></td>
                                            <td><a href="clients/view/'.$pay->client_id.'">'.$pay->client_name.'</a></td>
                                            <td>€'.$pay->importo.'</td>
                                            <td>'.$data_scadenza.'</td>
                                            <td id="pay'.$pay->id.'Sscadenza">'.$data_pagamento.'</td>
                                            <td>';
                                            if($pay->payment_type_id===1){
                                                echo '<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Pagamento ricorrente in ritardo, per gestirlo vai nella tab dei pagamenti ricorrenti più in basso.">____<span class="glyphicon glyphicon-info-sign"></span>____</button>';
                                            } else{
                                                echo $this->Form->input($pay->id, ['empty' => true, 'label' => false, 'value' =>$pay->numero_fattura,
                                                                                'id' => 'pay'.$pay->id.'Sfat', 'style' => 'width:100px', 'class' => 'num_ft']);
                                            }
                                            echo '</td>
                                            <td><div class="col-lg-6" id="pay'.$pay->id.'Spayed">';
                                                echo '<span class="glyphicon glyphicon-remove"></span></div>';
                                                if($pay->payment_type_id!=1){
                                                    echo '<div class="col-lg-6"><input class="checkpayS" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                                                }
                                            echo'</td>
                                        </tr>';
                                }
                            }
                if(isset($payScadenza[0])){
                foreach ($payScadenza as $pay){
                    (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'd-m-Y');
                    (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                    ($pay->payed)? $paneltype = 'success' : $paneltype = 'warning';
                    echo '<tr class="row-'.$paneltype.'">
                            <td><a href="contracts/view/'.$pay->contract_id.'">'.$pay->contract_name.'</a></td>                    
                            <td><a href="agents/view/'.$pay->agent_id.'">'.$pay->agent_name.'</a></td>
                            <td><a href="agents/view/'.$pay->client_id.'">'.$pay->client_name.'</a></td>
                            <td>€ '.$pay->importo.'</td>
                            <td>'.$data_scadenza.'</td>
                            <td id="pay'.$pay->id.'scadenza">'.$data_pagamento.'</td>
                            <td>';
                            if($pay->payed){
                                echo $this->Form->input($pay->id, ['disabled'=>true, 'empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                'id' => 'pay'.$pay->id.'fat', 'style' => 'width:100px', 'class' => 'num_ft']);
                            } else{
                                if($pay->payment_type_id===1){
                                    echo '<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pagamento ricorrente non pagato, per gestirlo vai nella tab dei pagamenti ricorrenti più in basso.">____<span class="glyphicon glyphicon-info-sign"></span>____</button>';
                                } else {
                                    echo $this->Form->input($pay->id, ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                'id' => 'pay'.$pay->id.'fat', 'style' => 'width:100px', 'class' => 'num_ft']);
                                }
                            }
                            echo '</td>
                            <td><div class="col-lg-6" id="pay'.$pay->id.'payed">';
                            if($pay->payed){
                                echo '<span class="glyphicon glyphicon-ok"></span>*</div>';
                                //<div class="col-lg-6"><input class="checkpay2" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'" checked></input></div>';
                            } else {
                                echo '<span class="glyphicon glyphicon-remove"></span></div>';
                                if($pay->payment_type_id!=1){
                                    echo '<div class="col-lg-6"><input class="checkpay2" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                                }
                            }
                            echo'</td>
                        </tr>';
                }
                }
                ?>
            </tbody>
            </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                            Pagamenti ricorrenti divisi per anno
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                    <?php
                        
                        $anno= 1900;
                        foreach($list as $pay){
                            if($pay->data_scadenza->year!=$anno){
                                $anno=$pay->data_scadenza->year;
                                if($anno===$now->year){
                                    echo '<li class="active"><a href="#'.$anno.'" data-toggle="tab">'.$anno.'</a></li>';
                                } else {
                                    echo '<li><a href="#'.$anno.'" data-toggle="tab">'.$anno.'</a></li>';
                                }
                                
                            }                            
                        }
                    ?>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                    <?php
                        
                        $anno= 1900;
                        $conta=0;
                        foreach($list as $pay){
                            if($pay->data_scadenza->year!=$anno){
                                if($conta!=0){
                                    echo '</tbody>
                                                        </table>
                                                    </div><!-- /. table-responsive-->
                                                    </div><!-- /. col-lg-12-->
                                                    
                                                </div><!-- /.panel-body -->
                                            </div><!-- /.collapse -->
                                        </div>'; //chiudo div precedenti
                                }
                                $conta=1;
                                $anno=$pay->data_scadenza->year;
                                $mese=0;
                                $totaleMese=0;
                                if($anno===$now->year){
                                    echo '<div class="tab-pane fade active in" id="'.$anno.'"><div class="panel-group" id="accordion'.$anno.'">'; 
                                } else {
                                    echo '<div class="tab-pane fade" id="'.$anno.'"><div class="panel-group" id="accordion'.$anno.'">'; 
                                }
                                echo '<div class="panel-body">
                                                   <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <table class="table" id="table-ric-'.$anno.'">
                                                            <thead>
                                                                <tr>
                                                                    <th>Contr/ord</th>
                                                                    <th>Cliente</th>
                                                                    <th>Agente</th>
                                                                    <th>Importo</th>
                                                                    <th>Scadenza</th>
                                                                    <th>Pagamento</th>
                                                                    <th>Ricorrenza</th>
                                                                    <th>Fattura</th>
                                                                    <th>Pagato</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>';
                            }      
                            if($pay->data_scadenza->year===$anno){
                                /*if($pay->data_scadenza->month!=$mese){
                                    if($mese!=0){
                                                    echo '</tbody>
                                                        </table>
                                                        </div><!-- /. table-responsive-->
                                                    </div><!-- /. col-lg-12-->
                                                    
                                                </div>
                                            </div>
                                        </div>';
                                    }
                                    $mese=$pay->data_scadenza->month;
                                    $totaleMese=0;
                                    echo '<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion'.$anno.'" href="#collapse'.$anno.$mese.'">'.$mesi[$mese-1].'</a>
                                                </h4>
                                            </div>';
                                            if($mese===$now->month){
                                                echo '<div id="collapse'.$anno.$mese.'" class="panel-collapse collapse in">';
                                            } else{
                                                echo '<div id="collapse'.$anno.$mese.'" class="panel-collapse collapse">';
                                            }
                                               echo '<div class="panel-body">
                                                   <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Contr/ordine</th>
                                                                    <th>Cliente</th>
                                                                    <th>Agente</th>
                                                                    <th>Importo</th>
                                                                    <th>Scadenza</th>
                                                                    <th>Pagamento</th>
                                                                    <th>Pagato</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>';
                                }*/
                                if(isset($contracts2[$pay->work_type_instance->contract_id]) || isset($orders2[$pay->work_type_instance->order_id])){
                                    //if($pay->data_scadenza->month===$mese){
                                        $totaleMese=$totaleMese+$pay->importo;
                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'd-m-Y');
                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                                         if($pay->payed){
                                            $class="row-success";
                                        }
                                        else{
                                            if($pay->data_scadenza<$now){
                                                $class="row-danger";
                                            } else{
                                                $class="row-warning";
                                            }
                                        }               
                                        echo '<tr class="'.$class.'">';
                                                                if(isset($contracts2[$pay->work_type_instance->contract_id])){
                                                                    echo '<td><a href="contracts/view/'.$pay->work_type_instance->contract_id.'">'.$contracts2[$pay->work_type_instance->contract_id].'</a></td>';
                                                                    echo '<td><a href="clients/view/'.$contractsClients[$pay->work_type_instance->contract_id].'">'.$clients[$contractsClients[$pay->work_type_instance->contract_id]].'</a></td>';
                                                                    echo '<td><a href="agents/view/'.$contractsAgents[$pay->work_type_instance->contract_id].'">'.$agents[$contractsAgents[$pay->work_type_instance->contract_id]].'</a></td>';
                                                                }
                                                                if(isset($orders2[$pay->work_type_instance->order_id])){
                                                                    echo '<td><a href="orders/view/'.$pay->work_type_instance->order_id.'">'.$orders2[$pay->work_type_instance->order_id].'</a></td>';
                                                                    echo '<td><a href="clients/view/'.$ordersClients[$pay->work_type_instance->order_id].'">'.$clients[$ordersClients[$pay->work_type_instance->order_id]].'</a></td>';
                                                                    echo '<td><a href="agents/view/'.$ordersAgents[$pay->work_type_instance->order_id].'">'.$agents[$ordersAgents[$pay->work_type_instance->order_id]].'</a></td>';
                                                                }
                                                                echo '<td>€ '.$pay->importo.'</td>
                                                                <td>'.$data_scadenza.'</td>
                                                                <td id="pay'.$pay->id.'Rscadenza">'.$data_pagamento.'</td>
                                                                <td>';
                                                                if($pay->payed){
                                                                    echo $this->Form->input($pay->id, ['disabled'=>true, 'options' => $mesi2, 'empty' => true, 'default' => $pay->ricorrenza,
                                                                                                                    'id' => 'pay'.$pay->id.'Rric', 'label' => false, 'style' => 'width:100px', 'class' => 'num_ft']);
                                                                }
                                                                else{
                                                                    echo $this->Form->input($pay->id, ['options' => $mesi2, 'empty' => true, 'default' => $pay->ricorrenza,
                                                                                                                    'id' => 'pay'.$pay->id.'Rric', 'label' => false, 'style' => 'width:100px', 'class' => 'num_ft']);
                                                                }
                                                                echo '</td>
                                                                <td>';
                                                                
                                                                    echo $this->Form->input($pay->id, ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                                                    'id' => 'pay'.$pay->id.'Rfat', 'style' => 'width:100px', 'class' => 'num_ft']);
                                                                
                                                                echo '</td>
                                                                <td><div class="col-lg-6" id="pay'.$pay->id.'Rpayed">';
                                                                if($pay->payed){
                                                                    echo '<span class="glyphicon glyphicon-ok"></span>*</div>';
                                                                } else {
                                                                    echo '<span class="glyphicon glyphicon-remove"></span></div>
                                                                    <div class="col-lg-6"><input class="checkpayRic" type="checkbox" name="pay'.$pay->id.'R" value="'.$pay->id.'"></input></div>';     
                                                                }
                                                              
                                                            echo '</td>
                                                            </tr>';
                                    //}
                                }
                            }
                        }
                    ?>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /. table-responsive-->
                                                    </div>
                                                    <!-- /. col-lg-12-->
                                                </div><!-- /.panel-body -->
                                            </div><!-- /.collapse -->
                                        </div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Provvigioni <?= $mesi[$now->month-1].' '.$now->year ?>
            </div>
            <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                                    <table class="table" id="table-index2">
                                        <thead>
                                            <tr>
                                                <th>Contr/Ord</th>
                                                <th>Agente</th>
                                                <th>Importo</th>
                                                <th>Scadenza</th>
                                                <th>Pagato</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                <?php
                if(isset($provvigioniScadenza[0])){
                foreach ($provvigioniScadenza as $pay){
                    (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'm-Y');
                    (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                    ($pay->payed)? $paneltype = 'success' : $paneltype = 'warning';
                    
                    echo '<tr class="row-'.$paneltype.'">
                            <td><a href="contracts/view/'.$pay->contract_id.'">'.$pay->contract_name.'</a></td>                    
                            <td><a href="agents/view/'.$pay->agent_id.'">'.$pay->agent_name.'</a></td>
                            <td>€ '.$pay->importo.'</td>
                            <td>'.$data_scadenza.'</td>
                            
                            <td><div class="col-lg-6" id="pay'.$pay->id.'payed">';
                            if($pay->payed){
                                echo '<span class="glyphicon glyphicon-ok"></span>*</div>
                                <div class="col-lg-6"><input class="checkpayPr" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'" checked></input></div>';
                            } else {
                                echo '<span class="glyphicon glyphicon-remove"></span></div>
                                <div class="col-lg-6"><input class="checkpayPr" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                            }
                            echo'</td>
                        </tr>';
                }
                }
                ?>
            </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
    
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Competenze <?= $now->year+1 ?> - Totale: <?= $totale_competenze ?>€
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="competenze-table" class="table table-striped">
                        <thead>
                            <tr>
                                <th>N.FT</th>
                                <th>Data FT</th>
                                <th>Cliente</th>
                                <th>Contr/Ord</th>
                                <th>Imponibile FT</th>
                                <th>Competenze <?= $now->year+1 ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                                                       
                            foreach($competenze as $competenza){
                                (!isset($competenza->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($competenza->data_pagamento, 'd-m-Y');
                                echo '<tr>
                                    <td>'.$competenza->numero_fattura.'</td>
                                    <td>'.$data_pagamento.'</td>
                                    <td><a href="clients/view/'.$competenza->client_id.'">'.$competenza->client_name.'</a></td>
                                    <td><a href="contracts/view/'.$competenza->contract_id.'">'.$competenza->contract_name.'</a></td>
                                    <td>€ '.$competenza->importo.'</td>
                                    <td>€ '.$competenza->importo.'</td>
                                    </tr>';
                                $totale_competenze = $totale_competenze + $competenza->importo;
                            }
                        ?>
                            
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>

    
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Pagamenti dell'anno corrente
            </div>
            <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-bar-chart"></div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
$this->Html->scriptStart(['block' => 'scriptBottom']); ?>
$(document).ready(function() {
    var ticks = [
    [0.5, "Gen"], [1.5, "Feb"], [2.5, "Mar"], [3.5, "Apr"], [4.5, "Mag"], [5.5, "Giu"], [6.5, "Lug"], [7.5, "Ago"], [8.5, "Set"], [9.5, "Ott"], [10.5, "Nov"], [11.5, "Dic"]
];
    var barOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 0.2
            }
        },
        xaxis: {
            ticks: ticks
        },
        grid: {
            hoverable: true
        },
        legend: {
            show: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };
    var d1_1 = [
        [0.1, <?=$payedThisYear[0]?>],
        [1.1, <?=$payedThisYear[1]?>],
        [2.1, <?=$payedThisYear[2]?>],
        [3.1, <?=$payedThisYear[3]?>],
        [4.1, <?=$payedThisYear[4]?>],
        [5.1, <?=$payedThisYear[5]?>],
        [6.1, <?=$payedThisYear[6]?>],
        [7.1, <?=$payedThisYear[7]?>],
        [8.1, <?=$payedThisYear[8]?>],
        [9.1, <?=$payedThisYear[9]?>],
        [10.1, <?=$payedThisYear[10]?>],
        [11.1, <?=$payedThisYear[11]?>]
    ];
     
    var d1_2 = [
        [0.3, <?=$delayedThisYear[0]?>],
        [1.3, <?=$delayedThisYear[1]?>],
        [2.3, <?=$delayedThisYear[2]?>],
        [3.3, <?=$delayedThisYear[3]?>],
        [4.3, <?=$delayedThisYear[4]?>],
        [5.3, <?=$delayedThisYear[5]?>],
        [6.3, <?=$delayedThisYear[6]?>],
        [7.3, <?=$delayedThisYear[7]?>],
        [8.3, <?=$delayedThisYear[8]?>],
        [9.3, <?=$delayedThisYear[9]?>],
        [10.3, <?=$delayedThisYear[10]?>],
        [11.3, <?=$delayedThisYear[11]?>]
    ];
     
    var d1_3 = [
        [0.5, <?=$notPayedThisYear[0]?>],
        [1.5, <?=$notPayedThisYear[1]?>],
        [2.5, <?=$notPayedThisYear[2]?>],
        [3.5, <?=$notPayedThisYear[3]?>],
        [4.5, <?=$notPayedThisYear[4]?>],
        [5.5, <?=$notPayedThisYear[5]?>],
        [6.5, <?=$notPayedThisYear[6]?>],
        [7.5, <?=$notPayedThisYear[7]?>],
        [8.5, <?=$notPayedThisYear[8]?>],
        [9.5, <?=$notPayedThisYear[9]?>],
        [10.5, <?=$notPayedThisYear[10]?>],
        [11.5, <?=$notPayedThisYear[11]?>]
    ];
    var barData = [
            {
            label: "Pagato", 
            data: d1_1,
            color: "#89A54E"
        },
        {
            label: "In ritardo", 
            data: d1_2,
            color: "#AA4643"
        },
        {
            label: "Da pagare", 
            data: d1_3,
            color: "#4572A7"
        }
    ];
    $.plot($("#flot-bar-chart"), barData, barOptions);
    $('#table-ric-2011').DataTable();
    $('#table-ric-2012').DataTable();
    $('#table-ric-2013').DataTable();
    $('#table-ric-2014').DataTable();
    $('#table-ric-2015').DataTable();
    $('#table-ric-2016').DataTable();
    $('#table-ric-2017').DataTable();
    $('#table-ric-2018').DataTable();
    $('#table-ric-2019').DataTable();
    $('#table-ric-2020').DataTable();
    $('#table-ric-2021').DataTable();
    $('#table-ric-2022').DataTable();
    $('#table-ric-2023').DataTable();
    $('#table-ric-2024').DataTable();
    $('#table-ric-2025').DataTable();
    $('#table-ric-2026').DataTable();
});
<?php
$this->Html->scriptEnd();
echo $this->fetch('scriptBottom'); 
?>