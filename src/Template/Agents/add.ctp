<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Aggiungi nuovo agente</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Inserimento dati agente
            </div>
            <div class="panel-body">
    <?= $this->Form->create($agent) ?>
                <?php $this->Form->templates($form_templates['fullForm']); ?>
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('nome'); ?>
                <?= $this->Form->input('codice'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('codice_fiscale'); ?>
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->input('provvigione_default'); ?>
                    </div>
                    <div class="col-lg-6">
                         <?= $this->Form->input('fisso_mensile'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('telefono'); ?>
                <?= $this->Form->input('note'); ?>
            </div>
            
        </div>
    
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-default']) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
