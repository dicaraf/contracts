<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Agenti</h1>
    </div>
    <div class="col-lg-6">
        <?= $this->Html->link('Aggiungi agente', ['controller' => 'Agents', 'action' => 'add'], ['class'=>'page-header pull-right']) ?>
    </div>
</div>
<div class="panel-body">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('codice') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('codice_fiscale') ?></th>
                <th><?= $this->Paginator->sort('telefono') ?></th>
                <th><?= $this->Paginator->sort('provvigione_default') ?></th>
                <th><?= $this->Paginator->sort('fisso_mensile') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($agents as $agent): ?>
            <tr>
                <td><?= h($agent->codice) ?></td>
                <td><?= h($agent->nome) ?></td>
                <td><?= h($agent->codice_fiscale) ?></td>
                <td><?= h($agent->telefono) ?></td>
                <td><?= h($agent->provvigione_default) ?>%</td>
                <td><?= h($agent->fisso_mensile) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $agent->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $agent->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $agent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agent->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
