<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header"><?= h('Agente '.$agent->codice) ?></h1>
    </div>
    <div class="col-lg-3">
        <?= $this->Html->link(__('Modifica Agente'), ['action' => 'edit', $agent->id], ['class'=>'page-header pull-right']) ?> 
    </div>
    <div class="col-lg-3">
        <?= $this->Form->postLink(__('Cancella Agente'), ['action' => 'delete', $agent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agent->id), 'class'=>'page-header pull-right']) ?> 
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    <table class="table table-striped">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($agent->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Codice') ?></th>
            <td><?= h($agent->codice) ?></td>
        </tr>
        <tr>
            <th><?= __('Codice Fiscale') ?></th>
            <td><?= h($agent->codice_fiscale) ?></td>
        </tr>
        <tr>
            <th><?= __('Telefono') ?></th>
            <td><?= h($agent->telefono) ?></td>
        </tr>
        <tr>
            <th><?= __('Provvigione di default (in %)') ?></th>
            <td><?= h($agent->provvigione_default) ?>%</td>
        </tr>
        <tr>
            <th><?= __('Fisso mensile') ?></th>
            <td><?= h($agent->fisso_mensile) ?></td>
        </tr>
        <tr>
            <th><?= __('Note') ?></th>
            <td><?= h($agent->note) ?></td>
        </tr>
    </table>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                            Anni
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                    <?php
                        
                        $anno= 1900;
                        foreach($list as $pay){
                          if(isset($contracts[$pay->work_type_instance->contract_id]) || isset($orders[$pay->work_type_instance->order_id])){
                            if($pay->data_scadenza->year!=$anno){
                                $anno=$pay->data_scadenza->year;
                                if($anno===$now->year){
                                    echo '<li class="active"><a href="#'.$anno.'" data-toggle="tab">'.$anno.'</a></li>';
                                } else {
                                    echo '<li><a href="#'.$anno.'" data-toggle="tab">'.$anno.'</a></li>';
                                }
                                
                            } 
                          }
                        }
                    ?>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                    <?php
                        $mesi = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'dicembre'];
                        $anno= 1900;
                        $conta=0;
                        foreach($list as $pay){
                          if(isset($contracts[$pay->work_type_instance->contract_id]) || isset($orders[$pay->work_type_instance->order_id])){
                            if($pay->data_scadenza->year!=$anno){
                                if($conta!=0){
                                    echo '</tbody>
                                                        </table>
                                                    </div><!-- /. table-responsive-->
                                                    </div><!-- /. col-lg-9-->
                                                    <div class="col-lg-3">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Totale mese</td>
                                                                        <td>'.$totaleMese.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Dati</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div><!-- /.panel-body -->
                                            </div><!-- /.collapse -->
                                        </div></div></div>'; //chiudo div precedenti
                                }
                                $conta=1;
                                $anno=$pay->data_scadenza->year;
                                $mese=0;
                                $totaleMese=0;
                                if($anno===$now->year){
                                    echo '<div class="tab-pane fade active in" id="'.$anno.'"><div class="panel-group" id="accordion'.$anno.'">'; 
                                } else {
                                    echo '<div class="tab-pane fade" id="'.$anno.'"><div class="panel-group" id="accordion'.$anno.'">'; 
                                }
                                
                            }      
                            if($pay->data_scadenza->year===$anno){
                                if($pay->data_scadenza->month!=$mese){
                                    if($mese!=0){
                                                    echo '</tbody>
                                                        </table>
                                                        </div><!-- /. table-responsive-->
                                                    </div><!-- /. col-lg-9-->
                                                    <div class="col-lg-3">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Totale mese</td>
                                                                        <td>'.$totaleMese.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Dati</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
                                    }
                                    $mese=$pay->data_scadenza->month;
                                    $totaleMese=0;
                                    echo '<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion'.$anno.'" href="#collapse'.$anno.$mese.'">'.$mesi[$mese-1].'</a>
                                                </h4>
                                            </div>';
                                            if($mese===$now->month){
                                                echo '<div id="collapse'.$anno.$mese.'" class="panel-collapse collapse in">';
                                            } else{
                                                echo '<div id="collapse'.$anno.$mese.'" class="panel-collapse collapse">';
                                            }
                                               echo '<div class="panel-body">
                                                   <div class="col-lg-9">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Contr/ord</th>
                                                                    <th>Tipo lavoro</th>
                                                                    <th>Importo</th>
                                                                    <th>Scadenza</th>
                                                                    <th>Pagamento</th>
                                                                    <th>Fatt</th>
                                                                    <th>Pagato</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>';
                                }
                                
                                    if($pay->data_scadenza->month===$mese){
                                        $totaleMese=$totaleMese+$pay->importo;
                                        if($pay->payed){
                                            $class="row-success";
                                        }
                                        else{
                                            if($pay->data_scadenza<$now){
                                                $class="row-danger";
                                            } else{
                                                $class="row-warning";
                                            }
                                        }
                                        (!isset($pay->data_scadenza)) ? $data_scadenza= '': $data_scadenza = date_format($pay->data_scadenza, 'm-Y');
                                        (!isset($pay->data_pagamento)) ? $data_pagamento= '': $data_pagamento = date_format($pay->data_pagamento, 'd-m-Y');
                                                        echo '<tr class="'.$class.'">';
                                                                if(isset($contracts[$pay->work_type_instance->contract_id])){
                                                                    echo '<td><a href="../../contracts/view/'.$pay->work_type_instance->contract_id.'">'.$contracts[$pay->work_type_instance->contract_id].'</a></td>';
                                                                }
                                                                if(isset($orders[$pay->work_type_instance->order_id])){
                                                                    echo '<td><a href="../../orders/view/'.$pay->work_type_instance->order_id.'">'.$orders[$pay->work_type_instance->order_id].'</a></td>';
                                                                }
                                                                echo '<td>'.$workTypes[$pay->work_type_instance->work_type_id].'</td>
                                                                <td>€ '.$pay->importo.'</td>
                                                                <td>'.$data_scadenza.'</td>
                                                                <td id="pay'.$pay->id.'scadenza">'.$data_pagamento.'</td>
                                                                <td>';
                                                            
                                                                echo $this->Form->input($pay->id, ['empty' => true, 'value' => $pay->numero_fattura, 'label' => false,
                                                                                'id' => 'pay'.$pay->id.'fat', 'style' => 'width:100px', 'class'=>'num_ft2']);
                                                            
                                                            echo '</td>
                                                                <td><div class="col-lg-6" id="pay'.$pay->id.'payed">';
                                                                if($pay->payed){
                                                                    echo '<span class="glyphicon glyphicon-ok"></span></div>
                                                                            <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'" checked></input></div>';
                                                                } else {
                                                                    echo '<span class="glyphicon glyphicon-remove"></span></div>
                                                                    <div class="col-lg-6"><input class="checkpay" type="checkbox" name="pay'.$pay->id.'" value="'.$pay->id.'"></input></div>';
                                                                          
                                                                }
                                                              
                                                            echo '</td>
                                                            </tr>';
                                    }
                                
                            }
                          }
                        }
                    ?>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /. table-responsive-->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Totale mese</td>
                                                                        <td><?= $totaleMese ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div><!-- /. col-lg-8-->
                                                </div><!-- /.panel-body -->
                                            </div><!-- /.collapse -->
                                        </div>
                            </div> <!-- /. panel-group-->
                        </div> <!-- /.tab-pane fade-->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    
    
    
</div>
