<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modifica agente</h1>
        <h4> <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $agent->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $agent->id)]
            )
        ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                  Modifica dati agente
            </div>
            <div class="panel-body">    
        <?= $this->Form->create($agent) ?>
        <?php $this->Form->templates($form_templates['fullForm']); ?>

    <fieldset>
       <div class="row">
            <div class="col-lg-4">
                <?= $this->Form->input('nome'); ?>
                <?= $this->Form->input('codice'); ?>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('codice_fiscale'); ?>
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->input('provvigione_default'); ?>
                    </div>
                    <div class="col-lg-6">
                         <?= $this->Form->input('fisso_mensile'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <?= $this->Form->input('telefono'); ?>
                <?= $this->Form->input('note'); ?>
            </div>
            
        </div>
    
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-default']) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>