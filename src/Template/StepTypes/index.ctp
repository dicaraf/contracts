<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="stepTypes index large-9 medium-8 columns content">
    <h3><?= __('Step Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('descrizione') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($stepTypes as $stepType): ?>
            <tr>
                <td><?= $this->Number->format($stepType->id) ?></td>
                <td><?= h($stepType->nome) ?></td>
                <td><?= h($stepType->descrizione) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $stepType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $stepType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $stepType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stepType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
