<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="stepTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($stepType) ?>
    <fieldset>
        <legend><?= __('Add Step Type') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('descrizione');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
