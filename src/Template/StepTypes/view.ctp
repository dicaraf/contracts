<nav class="navbar-default sidebar" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Step Type'), ['action' => 'edit', $stepType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Step Type'), ['action' => 'delete', $stepType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stepType->id)]) ?> </li>
        <?php if ($this->element('menu')){
            echo $this->element('menu');
        }
        ?>
    </ul>
</nav>
<div class="stepTypes view large-9 medium-8 columns content">
    <h3><?= h($stepType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($stepType->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Descrizione') ?></th>
            <td><?= h($stepType->descrizione) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($stepType->id) ?></td>
        </tr>
    </table>
</div>
