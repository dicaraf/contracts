<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkTypeInstances Entity.
 *
 * @property int $id
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property int $contract_id
 * @property \App\Model\Entity\Contract $contract
 * @property int $work_type_id
 * @property \App\Model\Entity\WorkType $work_type
 * @property float $importo
 * @property int $saldato
 * @property string $descrizione
 * @property \Cake\I18n\Time $data_inizio
 */
class workTypeInstances extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
