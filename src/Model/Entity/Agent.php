<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Agent Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $cognome
 * @property string $codice_fiscale
 * @property string $telefono
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 * @property string $note
 * @property \App\Model\Entity\Contract[] $contracts
 */
class Agent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
