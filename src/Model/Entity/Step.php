<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Step Entity.
 *
 * @property int $id
 * @property int $contract_id
 * @property \App\Model\Entity\Contract $contract
 * @property int $stepType_id
 * @property \App\Model\Entity\StepType $step_type
 * @property float $importo
 * @property float $provvigione_agent
 * @property float $fisso_agent
 * @property string $dati
 * @property \Cake\I18n\Time $inizio_step
 * @property \Cake\I18n\Time $fine_step
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 * @property \App\Model\Entity\Payment[] $payments
 */
class Step extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
