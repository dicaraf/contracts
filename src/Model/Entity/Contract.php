<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contract Entity.
 *
 * @property int $id
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property string $numero
 * @property float $importo
 * @property string $note
 * @property int $agent_id
 * @property \App\Model\Entity\Agent $agent
 * @property int $client_id
 * @property \App\Model\Entity\Client $client
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property float $provvigione
 * @property float $bonus
 * @property \App\Model\Entity\Step[] $steps
 * @property \App\Model\Entity\WorkType[] $work_types
 */
class Contract extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
