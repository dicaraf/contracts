<?php
namespace App\Model\Table;

use App\Model\Entity\Status;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Statuses Model
 *
 * @property \Cake\ORM\Association\HasMany $Contracts
 * @property \Cake\ORM\Association\HasMany $WorkTypeInstances
 */
class StatusesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('statuses');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Contracts', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('WorkTypeInstances', [
            'foreignKey' => 'status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('codice');

        return $validator;
    }
}
