<?php
namespace App\Model\Table;

use App\Model\Entity\Order;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\I18n\Time;

/**
 * Orders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Statuses
 * @property \Cake\ORM\Association\BelongsTo $Agents
 * @property \Cake\ORM\Association\BelongsTo $Clients
 * @property \Cake\ORM\Association\HasMany $Steps
 * @property \Cake\ORM\Association\BelongsToMany $WorkTypes
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders');
        $this->displayField('numero');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Statuses', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agent_id'
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id'
        ]);

        $this->hasMany('WorkTypeInstances', [
            'foreignKey' => 'order_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->requirePresence('note', 'create')
            ->allowEmpty('note');

        $validator
            ->decimal('provvigione')
            ->allowEmpty('provvigione');

        $validator
            ->decimal('bonus')
            ->allowEmpty('bonus');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['status_id'], 'Statuses'));
        $rules->add($rules->existsIn(['agent_id'], 'Agents'));
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        return $rules;
    }
    
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if(isset($data['work_type_instances'])){
            $importoContratto = 0;
            $contractClosed = 1;
            foreach ($data['work_type_instances'] as $workType){
                $importoWork = 0;
                $payments=null;
                $chiuso = 1;
                
                foreach ($workType['payments'] as $pay){
                    $pay['provvigione'] = $data['provvigione'];
                    if($pay['payment_type_id']!=3){
                        $pay['payment_type_id'] = $workType['payment_type_id'];
                    }
                    $payments[] = $pay;
                    if($pay['payment_type_id']!=1 && $pay['payment_type_id']!=3){    //se non ricorrente o provvigione
                        
                        $importoWork = $importoWork + $pay['importo'];
                        if($pay['payed'] != 1){   //se c'è almeno un non pagato
                            $chiuso = 0;
                        }
                    }
                    elseif($pay['payment_type_id']!=3){
                        $importoWork = $pay['importo'];
                        if($pay['payed'] != 1){   //se c'è almeno un non pagato
                            $chiuso = 0;
                        }
                    }
                }
                    if($chiuso!=0){
                        $workType['status_id'] = 2;  //stato chiuso
                        $workType['saldato'] = 1;
                    }
                    else{
                        $workType['saldato'] = 0;
                    }

                if($workType['status_id']!=2){
                    $contractClosed = 0;
                }
                $workType['payments'] = $payments;
                $workType['importo'] = $importoWork;
                $workType['provvigione'] = $data['provvigione'];
                $workTypes[] = $workType;
                $importoContratto = $importoContratto + $importoWork;
            }
            if($contractClosed){
                $data['status_id']=2; //contratto chiuso
            }
            $data['importo']=$importoContratto;
            $data['work_type_instances']=$workTypes;

        if(isset($data['data_contratto'])){
            $d = new Time($data['data_contratto']);
            $data['data_contratto'] = $d;
        }
        }

    }
    
    /*public function beforeSave(Event $event, $entity, $options)
    { 
        pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);

    }*/
    
    public function afterSave(Event $event, $entity, $options)
    {       
        /*pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);
        exit;*/
        
        foreach ($entity->work_type_instances as $workType){
            $workTypeInstanceTable = TableRegistry::get('WorkTypeInstances');
            if(isset($workType->id)){
                $workTypeInstance = $workTypeInstanceTable->get($workType->id);
            }
            else{
                $workTypeInstance = $workTypeInstanceTable->newEntity();
            }
            
            

            $type=$workType->toArray();
            $workTypeInstance = $this->WorkTypeInstances->patchEntity($workTypeInstance, $type);
            $workTypeInstance->order_id = $entity->id;
            


           /* if ($this->WorkTypeInstances->save($workTypeInstance)) {

            } else {
            }*/
        }
                   
    }
    
    public function findOpen(Query $query)
    {
        $query->matching('WorkTypeInstances.Payments', function($q){
            $q->where(['Payments.pagato' => false]);
            return $q;
        })
        ->distinct(['Orders.id']);
        $list = $query->toArray();
        foreach ($list as $contract){
            $array[] = $contract->id;
        }
        return $array;
    }
}
