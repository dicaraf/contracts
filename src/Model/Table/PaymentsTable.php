<?php
namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use ArrayObject;
use Cake\ORM\TableRegistry;

/**
 * Payments Model
 *
 * @property BelongsTo $Steps
 */
class PaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('payments');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('WorkTypeInstances', [
            'foreignKey' => 'work_type_instance_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('PaymentTypes', [
            'foreignKey' => 'payment_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('data_inserimento')
            ->allowEmpty('data_inserimento');
        
        $validator
            ->dateTime('data_scadenza')
            ->allowEmpty('data_scadenza');
        
        $validator
            ->dateTime('data_pagamento')
            ->allowEmpty('data_pagamento');
        
        $validator
            ->allowEmpty('numero_fattura');

        $validator
            ->decimal('importo')
            ->requirePresence('importo', 'create')
            ->notEmpty('importo');

        $validator
            ->boolean('payed')
            ->requirePresence('payed', 'create')
            ->notEmpty('payed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['work_type_instance_id'], 'WorkTypeInstances'));
        $rules->add($rules->existsIn(['payment_type_id'], 'PaymentTypes'));
        return $rules;
    }
    
    public function beforeMarshal(Event $event, $entity, $options)
    { 
        /*pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);*/
        if(isset($entity['data_inserimento'])){
            $d1 = new Time($entity['data_inserimento']);
            $entity['data_inserimento'] = $d1;
        }
        else{
            $entity['data_inserimento'] = Time::now();
        }
        if(isset($entity['data_scadenza'])){
            $d2 = new Time($entity['data_scadenza']);
            $entity['data_scadenza'] = $d2;
        }
        if(isset($entity['data_pagamento'])){
            $d3 = new Time($entity['data_pagamento']);
            $entity['data_pagamento'] = $d3;
        }
        /*pr(__FUNCTION__.' in '.__FILE__.'2');
        pr($entity);*/
        
    }
    
    /*public function beforeSave(Event $event, $entity, $options)
    { 
        pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);
        

    }*/
    
    public function afterSave(Event $event, $entity, $options)
    { 
        /*pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);*/
        if($entity->dirty('payed') && $entity->payed && $entity->payment_type_id === 1){ //se cambiato in pagato un ricorrente
            $array=$entity->toArray();
            $paymentTable = TableRegistry::get('Payments');
            $provvigione = $paymentTable->newEntity();
            $provvigione = $this->patchEntity($provvigione, $array);
            /*pr($provvigione);
            exit;*/
            $now2=date('t-m-Y',strtotime($provvigione->data_pagamento));
            $now2=new Time($now2);
            $provvigione->data_scadenza = $now2;
            $provvigione->data_pagamento = null;
            $provvigione->numero_fattura = null;
            $provvigione->importo = $provvigione->importo*$entity->provvigione/100;
            $provvigione->payment_type_id = 3;
            $provvigione->payed=0;
            $this->save($provvigione);
            if(isset($entity->ricorrenza)){
                $payment = $paymentTable->newEntity();
                $payment = $this->patchEntity($payment, $array);
                $data = $payment->data_scadenza;
                $payment->numero_fattura = null;
                $payment->data_scadenza = $data->addMonth($payment->ricorrenza);
                $payment->data_pagamento = null;
                $payment->payed=0;
                if($data!=$payment->data_scadenza){
                    $this->save($payment);
                }
                
            }
            
        }
    }
    
    public function findInScadenza($query, array $options)
    {      
        $str = $options['str'];
        return $query->where(function ($exp) use ($str) {
                return $exp->between('data_scadenza', new DateTime(), new DateTime($str));
                });
    }
    
    
}