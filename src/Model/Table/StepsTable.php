<?php
namespace App\Model\Table;
use App\Model\Entity\Step;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Steps Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contracts
 * @property \Cake\ORM\Association\BelongsTo $StepTypes
 * @property \Cake\ORM\Association\HasMany $Payments
 */
class StepsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('steps');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        /*$this->belongsTo('Contracts', [
            'foreignKey' => 'contract_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('Payments', [
            'foreignKey' => 'step_id'
        ]);*/
        $this->belongsTo('StepTypes', [
            'foreignKey' => 'stepType_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('importo')
            ->requirePresence('importo', 'create')
            ->notEmpty('importo');

        $validator
            ->decimal('provvigione_agent')
            ->requirePresence('provvigione_agent', 'create')
            ->notEmpty('provvigione_agent');

        $validator
            ->requirePresence('dati', 'create')
            ->notEmpty('dati');

        $validator
            ->dateTime('inizio_step')
            ->allowEmpty('inizio_step');

        $validator
            ->dateTime('fine_step')
            ->allowEmpty('fine_step');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['contract_id'], 'Contracts'));
        $rules->add($rules->existsIn(['stepType_id'], 'StepTypes'));
        return $rules;
    }
    
   /* public function afterSave(Event $event, $entity, $options)
    {        
        if($entity->isNew()){
            $paymentTable = TableRegistry::get('Payments');
            $payment = $paymentTable->newEntity();
            $payment->step_id = $entity->id;
            $payment->inEntrata = 1;
            $payment->importo = $entity->importo;
            $payment2 = $paymentTable->newEntity();
            $payment2->step_id = $entity->id;
            $payment2->importo = $entity->importo*$entity->provvigione_agent/100;

            if ($this->Payments->save($payment) && $this->Payments->save($payment2)) {
            } else {
            }
        }
        
    }*/
}
