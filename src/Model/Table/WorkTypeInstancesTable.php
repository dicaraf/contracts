<?php
namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\I18n\Time;

/**
 * WorkTypeInstances Model
 *
 * @property BelongsTo $Statuses
 * @property BelongsTo $Contracts
 * @property BelongsTo $WorkTypes
 */
class WorkTypeInstancesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('work_type_instances');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->hasMany('Payments', [
            'foreignKey' => 'work_type_instance_id'
        ]);
        
        $this->belongsTo('Statuses', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contracts', [
            'foreignKey' => 'contract_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('WorkTypes', [
            'foreignKey' => 'work_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PaymentTypes', [
            'foreignKey' => 'payment_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('importo')
            ->allowEmpty('importo');

        $validator
            ->boolean('saldato')
            ->allowEmpty('saldato');

        $validator
            ->allowEmpty('descrizione');
        $validator
            ->allowEmpty('order_id');
        $validator
            ->allowEmpty('contract_id');

        $validator
            ->allowEmpty('data_inizio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['status_id'], 'Statuses'));
        $rules->add($rules->existsIn(['contract_id'], 'Contracts'));
        $rules->add($rules->existsIn(['work_type_id'], 'WorkTypes'));
        $rules->add($rules->existsIn(['payment_type_id'], 'PaymentTypes'));
        return $rules;
    }
    
    /*public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        pr(__FUNCTION__.' in '.__FILE__);
        pr($data);
        
        /*if(!empty($data['data_inizio'])){
            $data_inizio="01-01-".(string)$data['data_inizio'];
            pr($data_inizio);
            $d1 = new Time($data_inizio);
            pr($d1);
            $data['data_inizio'] = $d1;
            pr($data['data_inizio']);
            pr($data);
        }
        
    }*/
    
    public function beforeSave(Event $event, $entity, $options)
    { 
        /*pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);
        exit;*/
        if($entity->dirty('saldato') && $entity->saldato && $entity->payment_type_id!=1){      //se saldato e non ricorrente
            $recente = new Time('1914-04-12 12:22:30');
            if(isset($entity->payments)){
            foreach ($entity->payments as $pay){
                if(!isset($pay['data_pagamento'])){
                    $pay['data_pagamento'] = Time::now();
                }
                $data_pagamento = new Time($pay['data_pagamento']);
                if($recente<$data_pagamento){
                    $recente=$data_pagamento; 
                }
            }
            }
            $newPay['importo'] = $entity->importo * $entity->provvigione/100;
            $newPay['payed'] = 0;
            $newPay['data_scadenza'] = $recente;
            $newPay['payment_type_id'] = 3;
            $entity->payments[] = $newPay;

            
            //pr($newPay);
            //pr($entity);
            //exit;
        }
        
    }
    
    public function afterSave(Event $event, $entity, $options)
    { 
        /*pr(__FUNCTION__.' in '.__FILE__);
        pr($entity);*/
        if(isset($entity->payments)){
        foreach ($entity->payments as $pay){
            $paymentTable = TableRegistry::get('Payments');
            if(isset($pay['id'])){
                $payment = $paymentTable->get($pay['id']);
            }
            else{
                $payment = $paymentTable->newEntity();
            }
            
            $payment = $this->Payments->patchEntity($payment, $pay);
            $payment->work_type_instance_id = $entity->id;
            if ($this->Payments->save($payment)) {
            } else {
            }
            
        }
        }
        
    }
    
    
}
