<?php
namespace App\Model\Table;

use App\Model\Entity\PaymentType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StepTypes Model
 *
 */
class PaymentTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('payment_types');
        $this->displayField('nome');
        $this->primaryKey('id');
        
        $this->hasMany('Payments', [
            'foreignKey' => 'payment_type_id'
        ]);
        $this->hasMany('WorkTypes', [
            'foreignKey' => 'payment_type_id'
        ]);
        $this->hasMany('WorkTypeInstances', [
            'foreignKey' => 'payment_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->integer('ricorrenza')
            ->allowEmpty('ricorrenza');

        return $validator;
    }
}
