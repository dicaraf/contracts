<?php
namespace App\Model\Table;

use App\Model\Entity\WorkType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WorkTypes Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Contracts
 */
class WorkTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('work_types');
        $this->displayField('nome');
        $this->primaryKey('id');
        
        $this->addBehavior('Timestamp');

        $this->belongsTo('PaymentTypes', [
            'foreignKey' => 'p_type_default',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('WorkTypeInstances', [
            'foreignKey' => 'work_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('codice', 'create')
            ->notEmpty('codice');

        $validator
            ->allowEmpty('descrizione');

        $validator
            ->integer('durata')
            ->allowEmpty('durata');

        return $validator;
    }
    
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['p_type_default'], 'PaymentTypes'));
        return $rules;
    }
}
