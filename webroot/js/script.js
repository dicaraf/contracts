$(document).ready(function(){
    
    $('[data-toggle="tooltip"]').tooltip();
    $('#competenze-table').DataTable();
    $('#table-index').DataTable();
    $('#table-index2').DataTable();
    $('#table-index3').DataTable({
        "order": [[ 7, "asc" ]]
    });
    
    $('body').on('focusout','.num_ft',function(){
    var id = $(this).attr('name');
    var fattura = $(this).val();
    var url = "payments/addft";
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura="+fattura,
        dataType: "html",
        success: function(msg) {
          $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    
    $('body').on('focusout','.num_ft2',function(){
    var id = $(this).attr('name');
    var fattura = $(this).val();
    var url = "../../payments/addft";
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura="+fattura,
        dataType: "html",
        success: function(msg) {
          $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    
    /*$(document).on("change", ".workTypeImporto", function() {
    var sum = 0;
    $(".workTypeImporto").each(function(){
        sum += +$(this).val();
    });
    $("#importo").val(sum);
    });*/
    //checkbox pagato in agente view
    $('body').on('change','.checkpay',function(){
    var id = $(this).val();
    var id2 = "#pay"+id+"fat";
    var fattura = $(id2).val();
    var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
    if(nome === 'add'){
        var url = "../Payments/pay";
    } else {
        var url = "../../Payments/pay";
    }
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura=" + fattura,
        dataType: "html",
        success: function(msg) {
            if(msg){
                $("#pay"+id+"scadenza").html(msg);
                $("#pay"+id+"payed").html("<span class='glyphicon glyphicon-ok'></span>");
                $(this).closest("tr").toggleClass( 'row-warning', false );
                $(this).closest("tr").toggleClass( 'row-danger', false );
                $(this).closest("tr").toggleClass( 'row-success', true );
            } else {
                $("#pay"+id+"scadenza").html(msg);
                $("#pay"+id+"payed").html("<span class='glyphicon glyphicon-remove'></span>");
                $(this).closest("tr").toggleClass( 'row-warning', true );
                $(this).closest("tr").toggleClass( 'row-success', false );
            }
           $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    //checkbox pagato in dashboard
    $('body').on('change','.checkpay2',function(){
    var id = $(this).val();
    var id2 = "#pay"+id+"fat";
    var fattura = $(id2).val();
    var url = "payments/pay";
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura=" + fattura,
        dataType: "html",
        success: function(msg) {
            $("#pay"+id+"scadenza").html(msg);
            $("#pay"+id+"payed").html("<span class='glyphicon glyphicon-ok'></span>*");
            $(this).attr('disabled', 'disabled');
            $(this).closest("tr").toggleClass( 'row-warning', false );
            $(this).closest("tr").toggleClass( 'row-danger', false );
            $(this).closest("tr").toggleClass( 'row-success', true );
        $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    //checkbox pagato provvigioni in dashboard
    $('body').on('change','.checkpayPr',function(){
    var id = $(this).val();
    var id2 = "#pay"+id+"fat";
    var fattura = $(id2).val();
    var url = "payments/pay";
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura=" + fattura,
        dataType: "html",
        success: function(msg) {
            if(msg){
                $("#pay"+id+"scadenza").html(msg);
                $("#pay"+id+"payed").html("<span class='glyphicon glyphicon-ok'></span>");
                $(this).closest("tr").toggleClass( 'row-warning', false );
                $(this).closest("tr").toggleClass( 'row-danger', false );
                $(this).closest("tr").toggleClass( 'row-success', true );
            } else {
                $("#pay"+id+"scadenza").html(msg);
                $("#pay"+id+"payed").html("<span class='glyphicon glyphicon-remove'></span>");
                $(this).closest("tr").toggleClass( 'row-warning', true );
                $(this).closest("tr").toggleClass( 'row-success', false );
            }
        $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    //checkbox pagato pagamenti scaduti in dashboard
    $('body').on('change','.checkpayS',function(){
    var id = $(this).val();
    var id2 = "#pay"+id+"Sfat";
    //var id3 = "#pay"+id+"Sric";
    var fattura = $(id2).val();
    //var ricorrenza = $(id3).val();
    var url = "payments/pay";
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura=" + fattura,
        dataType: "html",
        success: function(msg) {
                $("#pay"+id+"Sscadenza").html(msg);
                $("#pay"+id+"Spayed").html("*<span class='glyphicon glyphicon-ok'></span>");
                $(this).attr('disabled', 'disabled');
                $(this).closest("tr").toggleClass( 'row-warning', false );
                $(this).closest("tr").toggleClass( 'row-danger', false );
                $(this).closest("tr").toggleClass( 'row-success', true );
        $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    //checkbox pagato pagamenti ricorrenti
    $('body').on('change','.checkpayRic',function(){
    var id = $(this).val();
    var id2 = "#pay"+id+"Rfat";
    var id3 = "#pay"+id+"Rric";
    var fattura = $(id2).val();
    var ricorrenza = $(id3).val();
    var url = "payments/pay";
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
        context: this,
        type: "POST",
        url: url,
        data: "id=" + id + "&fattura=" + fattura + "&ricorrenza=" + ricorrenza,
        dataType: "html",
        success: function(msg) {
                $("#pay"+id+"Rscadenza").html(msg);
                $("#pay"+id+"Rpayed").html("<span class='glyphicon glyphicon-ok'></span>*");
                $(this).attr('disabled', 'disabled');
                $(this).closest("tr").toggleClass( 'row-warning', false );
                $(this).closest("tr").toggleClass( 'row-danger', false );
                $(this).closest("tr").toggleClass( 'row-success', true );
        $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    
    $('#worktype-str').keyup(function(){
    var str = $(this).val();
    var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
    if(nome === 'add'){
        var url = "../WorkTypes/getNomi";
    } else {
        var url = "../../WorkTypes/getNomi";
    }
    $('#wait-ajax').css('visibility', 'visible');
    $.ajax({
          type: "POST",
          url: url,
          data: "worktype-str=" + str,
          dataType: "html",
          success: function(msg) {
            $("#worktype_result").html(msg);
            $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
    });
    //$('#wait-ajax').css('visibility', 'hidden');
    });
    var counter = parseInt($('#counter').html());
    $('body').on('click', '.worktype_select', function(){
        var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
        if(nome === 'add'){
        var url = "../WorkTypeInstances/add_in_contract";
        } else {
        var url = "../../WorkTypeInstances/add_in_contract";
        }
        var valore = $(this).val();
        var testo = $(this).text();
        $('#wait-ajax').css('visibility', 'visible');
        $.ajax({
          type: "POST",
          url: url,
          data: "workTypeId=" + valore + "&counter=" + counter,
          dataType: "html",
          success: function(msg) {
            $( "<div class='panel panel-default' id='contractworktype" + counter +"'></div>" ).prependTo( "#accordion" );
            $("#contractworktype" + counter).html(msg);
            counter++;
            $('[data-toggle="tooltip"]').tooltip();
            $('.data_normale').datetimepicker({
                language:  'it',
                format: 'dd-mm-yyyy',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0
           });
           $('.data_inizio').datetimepicker({
                language:  'it',
                format: 'yyyy',
                weekStart: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 4,
                minView: 4,
                forceParse: 0
            });
          $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
              $(this).removeAttr('checked');
          }
        });
        
        /*var options = $('#worktype option');
        var values = $.map(options ,function(option) {
                return option.value;
        });
        if(values.indexOf(valore.toString()) === -1){
            $('#worktype').append($('<option>', {
            value: valore,
            text: testo,
            selected: true
            }));
        }*/
            
    });
    
    $('body').on('click', '#removeWorkType', function(){
        var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
        if(nome === 'add'){
        var url = "../WorkTypeInstances/delete";
        } else {
        var url = "../../WorkTypeInstances/delete";
        }
        var valore = parseInt($(this).attr("name"));
        $('#wait-ajax').css('visibility', 'visible');
        if(valore!==null){
          $.ajax({
            context: this,
            type: "POST",
            url: url,
            data: "id=" + valore,
            dataType: "html",
            success: function(msg) {
                $(this).closest('.panel-default').remove();
                $('#wait-ajax').css('visibility', 'hidden');
            },
            error: function() {
                $(this).closest('.panel-default').remove();
                $('#wait-ajax').css('visibility', 'hidden');
          
            }
            });
        }
        else{
            $(this).closest('.panel-default').remove();
            $('#wait-ajax').css('visibility', 'hidden');
        }
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    
    $('body').on('click', '.removePayment', function(){
        var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
        if(nome === 'add'){
        var url = "../Payments/delete";
        } else {
        var url = "../../Payments/delete";
        }
        var valore = parseInt($(this).attr("value"));
        var nome = $(this).attr("name");
        $('#wait-ajax').css('visibility', 'visible');
        if(valore!==null){
          $.ajax({
            context: this,
            type: "POST",
            url: url,
            data: "id=" + valore,
            dataType: "html",
            success: function(msg) {
                $("#"+nome).remove();
                $("#"+nome+"li").remove();
                $('#wait-ajax').css('visibility', 'hidden');
            },
            error: function() {
                $("#"+nome).remove();
                $("#"+nome+"li").remove();
                $('#wait-ajax').css('visibility', 'hidden');
            }
            });
        }
        else{
            $("#"+nome).remove();
            $("#"+nome+"li").remove();
            $('#wait-ajax').css('visibility', 'hidden');
        }
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    
    $('body').on('click', '.addPay', function(){
        var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
        if(nome === 'add'){
        var url = "../Payments/add_in_contract";
        } else {
        var url = "../../Payments/add_in_contract";
        }
        var counterT = parseInt($(this).attr("name"));
        var counterPay = parseInt($(this).attr("value"));
        $('#wait-ajax').css('visibility', 'visible');
        $.ajax({
          context: this,
          type: "POST",
          url: url,
          data: "counterT=" + counterT + "&counterPay=" + counterPay,
          dataType: "html",
          success: function(msg) {
            $( "<li><a href='#"+ counterT + 'pay' + counterPay +"' data-toggle='tab' aria-expanded='true' id='" +counterT+"pay"+counterPay+"li'>Nuovo Pagam</a></li>" ).insertBefore( $(this) );
            $( "<div class='tab-pane fade' id='" +counterT+"pay"+counterPay+"'></div>" ).appendTo( "#tab-content" + counterT +"" );
            $("#" +counterT+"pay"+counterPay+"").html(msg);
            counterPay++;
            $(this).attr("value", ""+counterPay+"");
            $('.data_normale').datetimepicker({
                language:  'it',
                format: 'dd-mm-yyyy',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0
            });
            $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
          }
        });
        //$('#wait-ajax').css('visibility', 'hidden');
    });
    
    $('body').on('change','form input:checkbox',function(){
        var id = $(this).attr("id");
        id1 = "#" + id + "day";
        id2 = "#" + id + "ric";
        id3 = "#" + id + "fat";
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = (day<10 ? '0' : '') + day + '-' +
            (month<10 ? '0' : '') + month + '-' +
            d.getFullYear();
        if($(id1).attr('disabled')){
            $(id1).removeAttr('disabled');
            $(id1).val(output);
           // $(id2).removeAttr('hidden');
            $(id2).css('visibility', 'visible');
        } else {
            $(id1).attr('disabled', 'disabled');
            $(id1).val('');
            $(id2).css('visibility', 'hidden');
            //$(id2).attr('hidden', 'hidden');
        }
        

        
    });
    
    $('#agent-id').change(function(){
    var id = $(this).find("option:selected").val();
    var nome = $(location).attr('pathname').substring($(location).attr('pathname').lastIndexOf('/') + 1);
    $('#wait-ajax').css('visibility', 'visible');
    if(nome === 'add'){
        $.ajax({
          type: "POST",
          url: "../Agents/getProvvigione",
          data: "agent-id=" + id,
          dataType: "html",
          success: function(msg) {
            $("#provvigione").val(msg);
          $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
          }
          });
    }
    else{
        $.ajax({
          type: "POST",
          url: "../../Agents/getProvvigione",
          data: "agent-id=" + id,
          dataType: "html",
          success: function(msg) {
            $("#provvigione").val(msg);
          $('#wait-ajax').css('visibility', 'hidden');
          },
          error: function() {
              $('#wait-ajax').css('visibility', 'hidden');
          }
          });
    }
  //$('#wait-ajax').css('visibility', 'hidden');
});

    $('.data_normale').datetimepicker({
        language:  'it',
        format: 'dd-mm-yyyy',
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0
    });
    
    $('.data_inizio').datetimepicker({
        language:  'it',
        format: 'yyyy',
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 4,
	minView: 4,
	forceParse: 0
    });
});

